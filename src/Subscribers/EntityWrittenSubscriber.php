<?php

namespace ShopPoints\Subscribers;

use ShopPoints\Core\Content\ShopPoint\ShopPointDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\EntityDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\Event\EntityWrittenContainerEvent;
use Shopware\Elasticsearch\Framework\Indexing\ElasticsearchIndexer;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class EntityWrittenSubscriber implements EventSubscriberInterface
{
    public function __construct(
        private readonly ElasticsearchIndexer $indexer,
        private readonly EntityDefinition $definition
    ) {
    }
    public static function getSubscribedEvents(): array
    {
        return [
            EntityWrittenContainerEvent::class => 'updateShopPointIndex'
        ];
    }

    public function updateShopPointIndex(EntityWrittenContainerEvent $event) {

        $updates = $event->getPrimaryKeys(ShopPointDefinition::ENTITY_NAME);

        if (empty($updates)) {
            return null;
        }

        $this->indexer->updateIds($this->definition, $updates);
    }
}