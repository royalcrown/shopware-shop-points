<?php

namespace ShopPoints\Subscribers;


use OpenSearchDSL\Sort\FieldSort;
use ShopPoints\Core\Content\ShopPoint\ShopPoint;
use ShopPoints\Core\Content\ShopPoint\ShopPointDefinition;
use ShopPoints\Core\Content\ShopPoint\Struct\ShopPointListingStruct;
use ShopPoints\elasticsearch\shopPoint\ShopPointSearchBuilder;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Sorting\FieldSorting;
use Shopware\Elasticsearch\Framework\DataAbstractionLayer\Event\ElasticsearchEntitySearcherSearchEvent;
use Shopware\Storefront\Page\Suggest\SuggestPageLoadedEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use OpenSearchDSL\Search;

class SuggestPageSubscriber implements EventSubscriberInterface
{
    public function __construct(
        private readonly EntityRepository $shopPointRepository,
        private readonly ShopPointSearchBuilder $shopPointSearchBuilder,
        private readonly RequestStack $request
    ) {
    }

    public static function getSubscribedEvents(): array
    {
        return [
            SuggestPageLoadedEvent::class => 'suggestPageLoadedEvent',
            ElasticsearchEntitySearcherSearchEvent::class => 'addGeoLocationSort',
        ];
    }

    public function suggestPageLoadedEvent(SuggestPageLoadedEvent $event): void
    {
        $page = $event->getPage();
        $context = $event->getContext();
        $request = $event->getRequest();

        $listingStruct = new ShopPointListingStruct();

        $criteria = $this->shopPointSearchBuilder->init();
        $this->shopPointSearchBuilder->addSearchCriteria($criteria, $request);
        $criteria->setLimit(2);

        $shopPoints = $this->shopPointRepository->search($criteria, $context);

        $listingStruct->setListing($shopPoints);

        $page->addExtension('shopPoints', $listingStruct);
    }

    public function addGeoLocationSort(ElasticsearchEntitySearcherSearchEvent $event): void
    {
        if (!($event->getDefinition() instanceof ShopPointDefinition)) {
            return;
        }

        $request = $this->request->getCurrentRequest();
        $orderKey = $request->get('order');
        $location = $request->cookies->get(ShopPoint::LOCATION_KEY);

        if ($location !== null && ($orderKey === ShopPoint::LOCATION_KEY || $orderKey === null)) {
            $this->addGeoSort(
                $event->getSearch(),
                $location,
                $request
            );
        }
    }

    private function addGeoSort(Search $search, string $location, Request $request):void
    {
        $search->addSort(new FieldSort('_geo_distance','asc', null, [
            'unit' => 'km',
            'coordinates' => $location,
        ]));

        $request->query->add(['order'=> ShopPoint::LOCATION_KEY]);
    }
}