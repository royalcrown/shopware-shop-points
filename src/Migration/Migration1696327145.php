<?php declare(strict_types=1);

namespace ShopPoints\Migration;

use Doctrine\DBAL\Connection;
use Shopware\Core\Framework\Migration\MigrationStep;

class Migration1696327145 extends MigrationStep
{
    public function getCreationTimestamp(): int
    {
        return 1696327145;
    }

    public function update(Connection $connection): void
    {
        $sql = <<<SQL
            CREATE TABLE IF NOT EXISTS `rc_shop_point` (
                `id` BINARY(16) NOT NULL,
                `name` VARCHAR(255) COLLATE utf8mb4_unicode_ci,
                `coordinates` VARCHAR(50) COLLATE utf8mb4_unicode_ci,
                `description` VARCHAR(255) COLLATE utf8mb4_unicode_ci,
                `active` TINYINT(1) COLLATE utf8mb4_unicode_ci,
                `media_id` BINARY(16) NULL,
                `created_at` DATETIME(3) NOT NULL,
                `updated_at` DATETIME(3),
                `cms_page_id` BINARY(16) NULL,
                `cms_page_version_id` BINARY(16) NULL,
                PRIMARY KEY (`id`),
                KEY `fk.shop.point.cms_page_id` (`cms_page_id`,`cms_page_version_id`),
                CONSTRAINT `fk.shop.point.cms_page_id` FOREIGN KEY (`cms_page_id`,`cms_page_version_id`) REFERENCES `cms_page` (`id`,`version_id`) ON DELETE SET NULL ON UPDATE CASCADE,
                CONSTRAINT `fk.shop_point.media_id` FOREIGN KEY (`media_id`) REFERENCES `media` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
            )
                ENGINE = InnoDB
                DEFAULT CHARSET = utf8mb4
                COLLATE = utf8mb4_unicode_ci;
            SQL;
        $connection->executeStatement($sql);


        $sql2 = <<<SQL
            CREATE TABLE IF NOT EXISTS `rc_shop_point_translation` (
              `rc_shop_point_id` BINARY(16) NOT NULL,
              `language_id` BINARY(16) NOT NULL,
              `meta_title` VARCHAR(255) COLLATE utf8mb4_unicode_ci NULL,
              `pack_unit` VARCHAR(255) COLLATE utf8mb4_unicode_ci NULL,
              `custom_fields` JSON NULL,
              `created_at` DATETIME(3) NOT NULL,
              `updated_at` DATETIME(3) NULL,
              `slot_config` JSON NULL,
              PRIMARY KEY (`rc_shop_point_id`, `language_id`),
              CONSTRAINT `json.shop_point_translation.custom_fields` CHECK (JSON_VALID(`custom_fields`)),
              CONSTRAINT `json.shop_point_translation.slot_config` CHECK (JSON_VALID(`slot_config`)),
              CONSTRAINT `fk.shop_point_translation.language_id` FOREIGN KEY (`language_id`)
                REFERENCES `language` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
              CONSTRAINT `fk.shop_point_translation.shop_point_id` FOREIGN KEY (`rc_shop_point_id`)
                REFERENCES `rc_shop_point` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
            SQL;


        $connection->executeStatement($sql2);

    }

    public function updateDestructive(Connection $connection): void
    {
        // implement update destructive
    }
}
