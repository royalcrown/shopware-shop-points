<?php declare(strict_types=1);

namespace ShopPoints\Migration;

use Doctrine\DBAL\Connection;
use Shopware\Core\Framework\Migration\MigrationStep;

class Migration1698154681 extends MigrationStep
{
    public function getCreationTimestamp(): int
    {
        return 1698154681;
    }

    public function update(Connection $connection): void
    {
        $database = $connection->fetchOne('select database();');

        $columnExist = $connection->fetchOne('SELECT 1 FROM information_schema.COLUMNS WHERE TABLE_NAME = \'rc_shop_point\' AND COLUMN_NAME = \'highlighted\' AND TABLE_SCHEMA = "' . $database . '";');

        if (!$columnExist) {
            $connection->executeStatement('ALTER TABLE rc_shop_point ADD COLUMN highlighted TINYINT(1)');
        }
    }

    public function updateDestructive(Connection $connection): void
    {
        // implement update destructive
    }
}
