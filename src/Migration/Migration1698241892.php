<?php declare(strict_types=1);

namespace ShopPoints\Migration;

use Doctrine\DBAL\Connection;
use Shopware\Core\Framework\Migration\MigrationStep;

class Migration1698241892 extends MigrationStep
{
    public function getCreationTimestamp(): int
    {
        return 1698241892;
    }

    public function update(Connection $connection): void
    {

        $sql = <<<SQL
CREATE TABLE IF NOT EXISTS `rc_opening_hours` (
    `id` BINARY(16) NOT NULL,
    `created_at` DATETIME(3) NOT NULL,
    `updated_at` DATETIME(3) NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
SQL;

        $connection->executeStatement($sql);
        $sql2 = <<<SQL
CREATE TABLE IF NOT EXISTS `rc_opening_hour` (
    `id` BINARY(16) NOT NULL,
    `opening_hours_id` BINARY(16) NULL,
    `start` VARCHAR(8) COLLATE utf8mb4_unicode_ci,
    `end` VARCHAR(8) COLLATE utf8mb4_unicode_ci,
    `created_at` DATETIME(3) NOT NULL,
    `updated_at` DATETIME(3) NULL,
    PRIMARY KEY (`id`),
    KEY `fk.rc_opening_hour.opening_hours_id` (`opening_hours_id`),
    CONSTRAINT `fk.rc_opening_hour.opening_hours_id` FOREIGN KEY (`opening_hours_id`) REFERENCES `rc_opening_hours` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
SQL;

        $connection->executeStatement($sql2);
        $database = $connection->fetchOne('select database();');
        $columnExist = $connection->fetchOne('SELECT 1 FROM information_schema.COLUMNS WHERE TABLE_NAME = \'rc_shop_point\' AND COLUMN_NAME = \'opening_hours_mon_id\' AND TABLE_SCHEMA = "' . $database . '";');
        if (!$columnExist) {
            $sql3 = <<<SQL
ALTER TABLE `rc_shop_point`
    ADD COLUMN `shop_point_media_id` BINARY(16) NULL,
    ADD COLUMN `street` VARCHAR(255) COLLATE utf8mb4_unicode_ci,
    ADD COLUMN `number` VARCHAR(32) COLLATE utf8mb4_unicode_ci,
    ADD COLUMN `zip_code` VARCHAR(32) COLLATE utf8mb4_unicode_ci,
    ADD COLUMN `city` VARCHAR(255) COLLATE utf8mb4_unicode_ci,
    ADD COLUMN `phone_number` VARCHAR(255) COLLATE utf8mb4_unicode_ci,
    ADD COLUMN `contact_email` VARCHAR(255) COLLATE utf8mb4_unicode_ci,
    ADD COLUMN `name_contact_person` VARCHAR(255) COLLATE utf8mb4_unicode_ci,
    ADD COLUMN `opening_hours_mon_id` BINARY(16) NULL,
    ADD COLUMN `opening_hours_tue_id` BINARY(16) NULL,
    ADD COLUMN `opening_hours_wed_id` BINARY(16) NULL,
    ADD COLUMN `opening_hours_thu_id` BINARY(16) NULL,
    ADD COLUMN `opening_hours_fri_id` BINARY(16) NULL,
    ADD COLUMN `opening_hours_sat_id` BINARY(16) NULL,
    ADD COLUMN `opening_hours_sun_id` BINARY(16) NULL,
    DROP FOREIGN KEY `fk.shop_point.media_id`,
    DROP COLUMN `media_id`,
    DROP COLUMN `description`,
    ADD KEY `fk.rc_shop_point.opening_hours_mon_id` (`opening_hours_mon_id`),
    ADD KEY `fk.rc_shop_point.opening_hours_tue_id` (`opening_hours_tue_id`),
    ADD KEY `fk.rc_shop_point.opening_hours_wed_id` (`opening_hours_wed_id`),
    ADD KEY `fk.rc_shop_point.opening_hours_thu_id` (`opening_hours_thu_id`),
    ADD KEY `fk.rc_shop_point.opening_hours_fri_id` (`opening_hours_fri_id`),
    ADD KEY `fk.rc_shop_point.opening_hours_sat_id` (`opening_hours_sat_id`),
    ADD KEY `fk.rc_shop_point.opening_hours_sun_id` (`opening_hours_sun_id`),
    ADD CONSTRAINT `fk.rc_shop_point.opening_hours_mon_id` FOREIGN KEY (`opening_hours_mon_id`) REFERENCES `rc_opening_hours` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
    ADD CONSTRAINT `fk.rc_shop_point.opening_hours_tue_id` FOREIGN KEY (`opening_hours_tue_id`) REFERENCES `rc_opening_hours` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
    ADD CONSTRAINT `fk.rc_shop_point.opening_hours_wed_id` FOREIGN KEY (`opening_hours_wed_id`) REFERENCES `rc_opening_hours` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
    ADD CONSTRAINT `fk.rc_shop_point.opening_hours_thu_id` FOREIGN KEY (`opening_hours_thu_id`) REFERENCES `rc_opening_hours` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
    ADD CONSTRAINT `fk.rc_shop_point.opening_hours_fri_id` FOREIGN KEY (`opening_hours_fri_id`) REFERENCES `rc_opening_hours` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
    ADD CONSTRAINT `fk.rc_shop_point.opening_hours_sat_id` FOREIGN KEY (`opening_hours_sat_id`) REFERENCES `rc_opening_hours` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
    ADD CONSTRAINT `fk.rc_shop_point.opening_hours_sun_id` FOREIGN KEY (`opening_hours_sun_id`) REFERENCES `rc_opening_hours` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
SQL;
            $connection->executeStatement($sql3);
        }
        $database = $connection->fetchOne('select database();');
        $tableExist = $connection->fetchOne('SELECT 1 FROM information_schema.COLUMNS WHERE TABLE_NAME = \'rc_shop_point_media\' AND TABLE_SCHEMA = "' . $database . '";');
        if (!$tableExist) {
            $connection->executeStatement('
                CREATE TABLE IF NOT EXISTS `rc_shop_point_media` (
                  `id` BINARY(16) NOT NULL,
                  `position` INT(11) NOT NULL DEFAULT 1,
                  `shop_point_id` BINARY(16) NOT NULL,
                  `media_id` BINARY(16) NOT NULL,
                  `custom_fields` JSON NULL,
                  `created_at` DATETIME(3) NOT NULL,
                  `updated_at` DATETIME(3) NULL,
                  PRIMARY KEY (`id`),
                  CONSTRAINT `json.shop_point_media.custom_fields` CHECK (JSON_VALID(`custom_fields`)),
                  CONSTRAINT `fk.shop_point_media.media_id` FOREIGN KEY (`media_id`)
                    REFERENCES `media` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
                  CONSTRAINT `fk.shop_point_media.shop_point_id` FOREIGN KEY (`shop_point_id`)
                    REFERENCES `rc_shop_point` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
            ');
            $connection->executeStatement('
            ALTER TABLE `rc_shop_point` ADD CONSTRAINT `fk.rc_shop_point.shop_point_media_id` FOREIGN KEY (`shop_point_media_id`) REFERENCES `rc_shop_point_media` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
        ');
            $connection->executeStatement('
            ALTER TABLE `rc_shop_point_translation` 
            ADD COLUMN `description` MEDIUMTEXT COLLATE utf8mb4_unicode_ci NULL
        ');

        }

    }

    public function updateDestructive(Connection $connection): void
    {
        // implement update destructive
    }
}
