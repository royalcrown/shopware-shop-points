<?php declare(strict_types=1);

namespace ShopPoints\Migration;

use Doctrine\DBAL\Connection;
use ShopPoints\Storefront\Page\ShopPoint\ShopPointPageLoader;
use Shopware\Core\Defaults;
use Shopware\Core\Framework\Migration\MigrationStep;
use Shopware\Core\Framework\Uuid\Uuid;

class Migration1698060697 extends MigrationStep
{
    private ?string $deDeLanguageId = null;

    public function getCreationTimestamp(): int
    {
        return 1698060697;
    }

    public function update(Connection $connection): void
    {
        $this->createShopPointCmsPage($connection);
    }

    public function updateDestructive(Connection $connection): void
    {
        // implement update destructive
    }

    private function createShopPointCmsPage(Connection $connection): void
    {
        $cmsPageId = Uuid::fromHexToBytes(ShopPointPageLoader::SHOP_POINT_LISTING);

        if($this->checkIfExists($connection, $cmsPageId)){
            return;
        }

        $languageEn = Uuid::fromHexToBytes(Defaults::LANGUAGE_SYSTEM);
        $languageDe = $this->getLanguageIdByLocale($connection, 'de-DE');
        $versionId = Uuid::fromHexToBytes(Defaults::LIVE_VERSION);


        // cms page
        $page = ['id' => $cmsPageId, 'type' => 'page', 'locked' => 1, 'created_at' => (new \DateTime())->format(Defaults::STORAGE_DATE_TIME_FORMAT)];
        $pageEng = [
            'cms_page_id' => $page['id'],
            'language_id' => $this->getLanguageIdByLocale($connection, 'en-GB'),
            'name' => 'Default shop point listing layout',
            'created_at' => (new \DateTime())->format(Defaults::STORAGE_DATE_TIME_FORMAT)
        ];
        $pageDeu = [
            'cms_page_id' => $page['id'],
            'language_id' => $languageDe,
            'name' => 'Standardlayout für Shop-Point-Einträge',
            'created_at' => (new \DateTime())->format(Defaults::STORAGE_DATE_TIME_FORMAT),
        ];

        $connection->insert('cms_page', $page);
        $connection->insert('cms_page_translation', $pageEng);
        $connection->insert('cms_page_translation', $pageDeu);


        $topSection = [
            'id' => Uuid::randomBytes(),
            'cms_page_id' => $page['id'],
            'position' => 0,
            'type' => 'default',
            'created_at' => (new \DateTime())->format(Defaults::STORAGE_DATE_TIME_FORMAT),
        ];

        $connection->insert('cms_section', $topSection);

        // cms blocks
        $blocks = [
            [
                'id' => Uuid::randomBytes(),
                'created_at' => (new \DateTime())->format(Defaults::STORAGE_DATE_TIME_FORMAT),
                'cms_section_id' => $topSection['id'],
                'locked' => 1,
                'position' => 0,
                'type' => 'shop-point-listing',
                'name' => 'Shop point listing',
                'margin_top' => '20px',
                'margin_bottom' => '20px',
                'margin_left' => '20px',
                'margin_right' => '20px',
                'background_media_mode' => 'cover',
            ],
        ];

        foreach ($blocks as $block) {
            $connection->insert('cms_block', $block);
        }

        // cms slots
        $slots = [
            ['id' => Uuid::randomBytes(), 'locked' => 1, 'cms_block_id' => $blocks[0]['id'], 'type' => 'shop-point-listing', 'slot' => 'content', 'created_at' => (new \DateTime())->format(Defaults::STORAGE_DATE_TIME_FORMAT), 'version_id' => $versionId],
        ];

        $slotTranslationData = [
            [
                'cms_slot_id' => $slots[0]['id'],
                'cms_slot_version_id' => $versionId,
                'language_id' => $languageEn,
                'created_at' => (new \DateTime())->format(Defaults::STORAGE_DATE_TIME_FORMAT),
                'config' => json_encode([
                    'boxLayout' => ['source' => 'static', 'value' => 'standard'],
                ]),
            ],
        ];

        $slotTranslations = [];
        foreach ($slotTranslationData as $slotTranslationDatum) {
            $slotTranslationDatum['language_id'] = $languageEn;
            $slotTranslations[] = $slotTranslationDatum;

            $slotTranslationDatum['language_id'] = $languageDe;
            $slotTranslations[] = $slotTranslationDatum;
        }

        foreach ($slots as $slot) {
            $connection->insert('cms_slot', $slot);
        }

        foreach ($slotTranslations as $translation) {
            $connection->insert('cms_slot_translation', $translation);
        }
    }

    private function getDeDeLanguageId(): string
    {
        if (!$this->deDeLanguageId) {
            $this->deDeLanguageId = Uuid::randomHex();
        }

        return $this->deDeLanguageId;
    }

    private function checkIfExists(Connection $connection, string $cmsPageId): bool
    {
        $invoiceId = $connection->fetchOne(
            'SELECT id FROM `cms_page` WHERE `id` = :id', ['id' => $cmsPageId]);

        return (bool) $invoiceId;
    }
    private function getLanguageIdByLocale(Connection $connection, string $locale): ?string
    {
        $sql = <<<'SQL'
SELECT `language`.`id`
FROM `language`
INNER JOIN `locale` ON `locale`.`id` = `language`.`locale_id`
WHERE `locale`.`code` = :code
SQL;

        $languageId = $connection->executeQuery($sql, ['code' => $locale])->fetchOne();
        if (!$languageId && $locale !== 'en-GB') {
            return null;
        }

        if (!$languageId) {
            return Uuid::fromHexToBytes(Defaults::LANGUAGE_SYSTEM);
        }

        return $languageId;
    }


}
