<?php

declare(strict_types=1);

namespace ShopPoints\DataResolver;

use ShopPoints\Core\Content\ShopPoint\ShopPoint;
use ShopPoints\Core\Content\ShopPoint\Struct\ShopPointListingStruct;
use ShopPoints\Core\Content\ShopPoint\Struct\ShopPointSortingStruct;
use ShopPoints\elasticsearch\shopPoint\ShopPointSearchBuilder;
use Shopware\Core\Content\Cms\Aggregate\CmsSlot\CmsSlotEntity;
use Shopware\Core\Content\Cms\DataResolver\Element\AbstractCmsElementResolver;
use Shopware\Core\Content\Cms\DataResolver\Element\ElementDataCollection;
use Shopware\Core\Content\Cms\DataResolver\ResolverContext\ResolverContext;
use Shopware\Core\Content\Cms\DataResolver\CriteriaCollection;
use Shopware\Core\Content\Product\SalesChannel\Sorting\ProductSortingEntity;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\Struct\ArrayStruct;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use Symfony\Component\HttpFoundation\RequestStack;

class ShopPointListingCmsElementResolver extends AbstractCmsElementResolver
{
    public function __construct(
        private readonly EntityRepository $shopPointRepository,
        private readonly RequestStack $requestStack,
        private readonly ShopPointSearchBuilder $shopPointSearchBuilder
    ) {
    }

    public function getType(): string
    {
        return 'shop-point-listing';
    }

    public function collect(CmsSlotEntity $slot, ResolverContext $resolverContext): ?CriteriaCollection
    {
        return null;
    }

    public function enrich(CmsSlotEntity $slot, ResolverContext $resolverContext, ElementDataCollection $result): void
    {
        $data = new ShopPointListingStruct();

        $context = $resolverContext->getSalesChannelContext()->getContext();
        $sortingConfig = $this->createSortingConfig();
        $criteria = $this->getShopPointListingCriteria($resolverContext->getSalesChannelContext(), $sortingConfig);

        $shopPoints = $this->shopPointRepository->search($criteria, $context);
        $shopPoints->addExtension('sorting', $sortingConfig);

        $data->setListing($shopPoints);
        $slot->setData($data);
    }

    private function getShopPointListingCriteria(
        SalesChannelContext $salesChannelContext,
        ShopPointSortingStruct $sortingConfig
    ): Criteria {
        $request =  $this->requestStack->getCurrentRequest();

        $criteria = $this->shopPointSearchBuilder->init();

        // first handle custom sorting before adding a possible default
        $this->shopPointSearchBuilder->handleSorting($criteria, $request, $sortingConfig);
        $this->shopPointSearchBuilder->addDefaultSorting($criteria, $request);
        $this->shopPointSearchBuilder->handlePagination($criteria, $salesChannelContext, $request);

        return $criteria;
    }

    private function createSortingConfig(): ShopPointSortingStruct
    {
        $sortingStruct = new ShopPointSortingStruct();
        $sort1 = new ProductSortingEntity();
        $sort1->setKey('name-asc');
        $sort1->setPriority(1);
        $sort1->setTranslated(['label'=>"rc-shop-point.listing.sort.name.aToz"]);
        $sort1->setFields([
            ['field' => 'name', 'order' => 'asc', 'priority' => 1, 'naturalSorting' => 1],
        ]);

        $sort2 = new ProductSortingEntity();
        $sort2->setKey('name-desc');
        $sort2->setPriority(2);
        $sort2->setTranslated(['label'=>"rc-shop-point.listing.sort.name.zToa"]);
        $sort2->setFields([
            ['field' => 'name', 'order' => 'desc', 'priority' => 1, 'naturalSorting' => 1],
        ]);

        $sort3 = new ProductSortingEntity();
        $sort3->setKey(ShopPoint::LOCATION_KEY);
        $sort3->setPriority(3);
        $sort3->setTranslated(['label'=>"rc-shop-point.listing.sort.location"]);
        $sort3->addExtension('accessibility', new ArrayStruct(['disabled' => !$this->hasLocationCookie()]));

        $sortingStruct->addSortingOption($sort1);
        $sortingStruct->addSortingOption($sort2);
        $sortingStruct->addSortingOption($sort3);

        return $sortingStruct;
    }

    private function hasLocationCookie(): bool
    {
        return (bool)$this->requestStack->getCurrentRequest()->cookies->get(ShopPoint::LOCATION_KEY);
    }
}
