<?php

namespace ShopPoints\DataResolver;

use ShopPoints\Core\Content\ShopPoint\ShopPoint;
use ShopPoints\Core\Content\ShopPoint\Struct\ShopPointMapStruct;
use Shopware\Core\Content\Cms\Aggregate\CmsSlot\CmsSlotEntity;
use Shopware\Core\Content\Cms\DataResolver\CriteriaCollection;
use Shopware\Core\Content\Cms\DataResolver\Element\AbstractCmsElementResolver;
use Shopware\Core\Content\Cms\DataResolver\Element\ElementDataCollection;
use Shopware\Core\Content\Cms\DataResolver\ResolverContext\ResolverContext;
use Shopware\Core\Content\Seo\SeoUrlPlaceholderHandlerInterface;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\System\SystemConfig\SystemConfigService;

class ShopPointMapCmsElementResolver extends AbstractCmsElementResolver
{
    public function __construct(
        private readonly EntityRepository $shopPointRepository,
        private readonly SystemConfigService $systemConfigService,
        private readonly SeoUrlPlaceholderHandlerInterface $seoUrlReplacer
    ) {

    }

    public function getType(): string
    {
        return 'shop-point-map';
    }

    public function collect(CmsSlotEntity $slot, ResolverContext $resolverContext): ?CriteriaCollection
    {
        return null;
    }

    public function enrich(CmsSlotEntity $slot, ResolverContext $resolverContext, ElementDataCollection $result): void
    {
        $pluginConfig = $this->systemConfigService->get('ShopPoints.config.ApiKey');
        $data = new ShopPointMapStruct();
        $config = $slot->getFieldConfig();
        $fields = $this->formatFieldInfo($config->getElements(), $pluginConfig);
        $data->setSettings($fields);
        $context = $resolverContext->getSalesChannelContext()->getContext();
        $criteria = new Criteria();
        $criteria->addAssociation('cover');
        $shopPoints = $this->shopPointRepository->search($criteria, $context);
        $shopPointsInfo = $this->formatShopPoint($shopPoints->getElements());
        $data->setShopPointsInfo($shopPointsInfo);
        $slot->setData($data);
    }

    /**
     * @param array $fields
     * @param string $pluginConfig
     * @return array
     */
    private function formatFieldInfo(array $fields, string $pluginConfig): array {
        $array = [];
        foreach($fields as $key => $field) {
            $array[$key] =  $field->getValue();
        };
        $array['apiKey'] = $pluginConfig;
        return $array;
    }
    /**
     * @param ShopPoint[] $shopPoints
     * @return array
     */
    private function formatShopPoint(array $shopPoints): array {
        $shopPointsInfo = [];
        foreach ($shopPoints as $shopPoint) {
            $shopPointsInfo[] = [
                'name' => $shopPoint->getName(),
                'url' => $this->seoUrlReplacer->generate('frontend.detail.shopPoint.page', ['shopPointId' => $shopPoint->getId()]),
                'desc'=> $shopPoint->getDescription(),
                'coords' => $shopPoint->getCoordinates(),
                'img' => $shopPoint->getCover()?->getMedia()->getUrl(),
                'place' => $shopPoint->getCity(),

            ];
        };
        return  $shopPointsInfo;
    }
}