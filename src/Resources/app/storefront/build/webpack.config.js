const { join, resolve } = require('path');

module.exports = () => {
    return {
        resolve: {
            alias: {
                '@maps': resolve(
                    join(__dirname, '..', 'node_modules', '@googlemaps')
                ),
                '@js-cookie': resolve(
                    join(__dirname, '..', 'node_modules', 'js-cookie')
                ),
            },
        }
    }
}