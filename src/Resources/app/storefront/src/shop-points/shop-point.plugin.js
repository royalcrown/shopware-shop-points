import Plugin from 'src/plugin-system/plugin.class';


export default class ShopPointPlugin extends Plugin {
   getPosition(position) {
        console.log(position.coords.latitude, position.coords.longitude);
    }
    init() {
        navigator.geolocation.getCurrentPosition(this.getPosition);
        console.log('in controller');
        window.addEventListener('scroll', this.onScroll.bind(this));
    }

    onScroll() {
        if ((window.innerHeight + window.pageYOffset) >= document.body.offsetHeight) {
            alert('Seems like there\'s nothing more to see here.');
        }
    }
}
