import ShopPointListingSortingPlugin from './shop-point-listing-sorting/shop-point-listing-sorting.plugin';
import ShopPointMapPlugin from "./shop-point-map/shop-point-map.plugin";

const PluginManager = window.PluginManager;
PluginManager.override('ListingSorting', ShopPointListingSortingPlugin, '[data-listing-sorting]');
PluginManager.register('ShopPointMap', ShopPointMapPlugin, '[data-shop-point-map]');