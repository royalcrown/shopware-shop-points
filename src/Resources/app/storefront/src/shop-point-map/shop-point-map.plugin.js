import Plugin from 'src/plugin-system/plugin.class';
import {Loader} from '@maps/js-api-loader';

let openMarker = null;
export default class ShopPointMapPlugin extends Plugin {
    init() {
        this.mapElement = document.getElementById('map');
        this.settings = JSON.parse(this.mapElement.dataset.settings);

        if(this.settings.apiKey) {
            this.loader = new Loader({
                apiKey: `${this.settings.apiKey}`,
                version: 'weekly',
                libraries: ['places', 'geometry', 'geocoding'],
            });
            this.intersectionObserver = new IntersectionObserver((entries) => {
                for (const entry of entries) {
                    if (entry.isIntersecting) {
                        entry.target.classList.add("drop");
                        this.intersectionObserver.unobserve(entry.target);
                    }
                }
            });
            this._getAllShopPoints();
        } else {
            console.log('WARNING: no api key was provided')
        }
    }
    _getAllShopPoints() {
        this.shopPoints = JSON.parse(this.mapElement.dataset.shoppoints);
        this.markers = [];
        this._initMap();
    }
    _initMap() {
        this.loader.load()
            .then(() => {
                this.getMap();
            });
    }
    async _getUtils() {
        //TODO: check for wishes styles
        this.styledMapType = new google.maps.StyledMapType(
            [
                { elementType: "geometry", stylers: [{ color: "#ebe3cd" }] },
                { elementType: "labels.text.fill", stylers: [{ color: "#523735" }] },
                { elementType: "labels.text.stroke", stylers: [{ color: "#f5f1e6" }] },
                {
                    featureType: "administrative",
                    elementType: "geometry.stroke",
                    stylers: [{ color: "#c9b2a6" }],
                },
                {
                    featureType: "administrative.land_parcel",
                    elementType: "geometry.stroke",
                    stylers: [{ color: "#dcd2be" }],
                },
                {
                    featureType: "administrative.land_parcel",
                    elementType: "labels.text.fill",
                    stylers: [{ color: "#ae9e90" }],
                },
                {
                    featureType: "landscape.natural",
                    elementType: "geometry",
                    stylers: [{ color: "#dfd2ae" }],
                },
                {
                    featureType: "poi",
                    elementType: "geometry",
                    stylers: [{ color: "#dfd2ae" }],
                },
                {
                    featureType: "poi",
                    elementType: "labels.text.fill",
                    stylers: [{ color: "#93817c" }],
                },
                {
                    featureType: "poi.park",
                    elementType: "geometry.fill",
                    stylers: [{ color: "#a5b076" }],
                },
                {
                    featureType: "poi.park",
                    elementType: "labels.text.fill",
                    stylers: [{ color: "#447530" }],
                },
                {
                    featureType: "road",
                    elementType: "geometry",
                    stylers: [{ color: "#f5f1e6" }],
                },
                {
                    featureType: "road.arterial",
                    elementType: "geometry",
                    stylers: [{ color: "#fdfcf8" }],
                },
                {
                    featureType: "road.highway",
                    elementType: "geometry",
                    stylers: [{ color: "#f8c967" }],
                },
                {
                    featureType: "road.highway",
                    elementType: "geometry.stroke",
                    stylers: [{ color: "#e9bc62" }],
                },
                {
                    featureType: "road.highway.controlled_access",
                    elementType: "geometry",
                    stylers: [{ color: "#e98d58" }],
                },
                {
                    featureType: "road.highway.controlled_access",
                    elementType: "geometry.stroke",
                    stylers: [{ color: "#db8555" }],
                },
                {
                    featureType: "road.local",
                    elementType: "labels.text.fill",
                    stylers: [{ color: "#806b63" }],
                },
                {
                    featureType: "transit.line",
                    elementType: "geometry",
                    stylers: [{ color: "#dfd2ae" }],
                },
                {
                    featureType: "transit.line",
                    elementType: "labels.text.fill",
                    stylers: [{ color: "#8f7d77" }],
                },
                {
                    featureType: "transit.line",
                    elementType: "labels.text.stroke",
                    stylers: [{ color: "#ebe3cd" }],
                },
                {
                    featureType: "transit.station",
                    elementType: "geometry",
                    stylers: [{ color: "#dfd2ae" }],
                },
                {
                    featureType: "water",
                    elementType: "geometry.fill",
                    stylers: [{ color: "#b9d3c2" }],
                },
                {
                    featureType: "water",
                    elementType: "labels.text.fill",
                    stylers: [{ color: "#92998d" }],
                },
            ],
            { name: "Styled Map" },
        );
        const { Map } = await google.maps.importLibrary("maps");
        const { AdvancedMarkerElement, PinElement } = await google.maps.importLibrary("marker");
        this.infoWindow = new google.maps.InfoWindow();
        this.CENTER = this._getCoords(this.settings.center, true);
        return {
            map: new Map(document.getElementById('map'), {
                zoom: Number(`${this.settings.zoom}`),
                center: this.CENTER,
                mapId: "COZINO_POINTS_MAP_ID",
                disableDefaultUI: true,
                mapTypeControlOptions: {
                    mapTypeIds: ["roadmap", "satellite", "hybrid", "terrain", "styled_map"]
                }
            }),
            AdvancedMarkerElement,
            PinElement,
            info: new google.maps.InfoWindow(),
        }
    }
    isInfoWindowOpen(infoWindow, isOpen){
        if (isOpen) {
            const map = infoWindow.getMap();
            return (map !== null && typeof map !== "undefined");
        }
        return isOpen
    }
    getMap() {
        this._getUtils().then(({map, AdvancedMarkerElement, PinElement, info}) => {
            map.mapTypes.set("styled_map", this.styledMapType);
            map.setMapTypeId(`${this.settings.mapType}`);
            this.map = map;
            this.infoWindow = info;
            google.maps.event.addListenerOnce(this.map, 'idle', () => {
                this.shopPoints.forEach((point) => {
                    this._createMarker(this.map, point, AdvancedMarkerElement, PinElement);
                });
            });
        });
    }
    _getCoords(point, forCenter = false) {
        let coords
        if(forCenter) {
            coords = point;
        } else {
            coords = point.coords;
        }

        return {lat: Number(coords.split(',')[0]), lng: Number(coords.split(',')[1])}
    }

    /**
     * @param {
     *     {name: string, coords: string, img: any, place?: string, desc?: string, url?: string}
     * } point
     */
    _buildContent(point) {
        const content = document.createElement("a");
        content.href = point.url;
        content.classList.add("details");
        content.style.width =  'fit-content';
        const details = document.createElement('div');
        details.classList.add('image-container');
        const description = point.desc ?? 'Geen omschrijving gevonden';
        if(point.img !== null) {
            details.innerHTML = `<img alt="${description}" src="${point.img}"/>`
        } else {
            details.innerHTML = `<span style="padding: 3px; margin:auto;">${description}</span>`
        }
        content.innerHTML = `
          ${details.outerHTML}
              <div class="text-container">
                <div class="name"> ${point.name}</div>
                <div class="place">${point.place ?? ''}</div>
              </div>
          `;
        return content;
    }

    _createMarker(map, point, AdvancedMarkerElement, PinElement) {
        const glyphImg = document.createElement("img");
        if (point.glyph) {
            glyphImg.src = point.glyph;
        }
        //TODO: glyph in pin check

        // USE GLYPH TO ADD CUSTOM LOGO FOR PIN
        // glyphImg = document.createElement("img");
        // glyphImg.src =
        //     "https://developers.google.com/maps/documentation/javascript/examples/full/images/google_logo_g.svg";

        const pinElement = new PinElement({
            background: `${this.settings.pinBackgroundColor}`,
            borderColor: `${this.settings.pinBorderColor}`,
        });

        if (glyphImg.src) {
            pinElement.glyph = glyphImg
        } else {
            pinElement.glyphColor = `${this.settings.pinGlyphColor}`;
        }
        const advMarker = new AdvancedMarkerElement({
            position: this._getCoords(point),
            map: map,
            title: point.name,
            content: pinElement.element
        });

        const content = advMarker.content;
        content.style.opacity = '0';
        content.addEventListener("animationend", () => {
            content.classList.remove("drop");
            content.style.opacity = "1";
        });

        content.addEventListener('click', () => {
            window.location.href = point.url;
        });
        content.addEventListener('mouseover', () => {
            if (!this.isInfoWindowOpen(this.infoWindow, openMarker === advMarker)) {
                this.infoWindow.setContent(this._buildContent(point));
                this.infoWindow.open(this.map, advMarker);
                openMarker = advMarker
                pinElement.scale = 1;
            }
        });
        content.addEventListener('mouseout', () => {
            pinElement.scale= 1;
        });

        const time = 0.5 + Math.random(); // 0.5s delay to see the animation

        content.style.setProperty("--delay-time", time + "s");
        this.intersectionObserver.observe(content);

    }
}