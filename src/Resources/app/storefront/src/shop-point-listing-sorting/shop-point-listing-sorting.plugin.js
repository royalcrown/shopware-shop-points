import ListingSortingPlugin from 'src/plugin/listing/listing-sorting.plugin';

//import googlemaps
import {Loader} from '@maps/js-api-loader';
import Cookies from '@js-cookie';

export default class ShopPointListingSortingPlugin extends ListingSortingPlugin {

    static options = {
        ...super.options,
        'zipcode': null,
        'shopPointParams': null,
    }

    init() {
        super.init();
        this.loader = new Loader({
            apiKey: 'AIzaSyBhU8rje-9P2zyXUIxVm2VshgxSMeHUog8',
            version: 'weekly',
            libraries: ['places', 'geometry', 'geocoding'],
        });

        this.zipCodeInput = this.el.querySelector('#autocomplete');
        this.currentLocationButton = this.el.querySelector('#sortCurrentLocationButton');
        this.loaderAnnimation = document.querySelector('.loading-block');
        this.listingWrapper = document.querySelector('.cms-element-product-listing')

        if(this.currentLocationButton) {
            this.currentLocationButton.addEventListener('click', () => {
                this.getCurrentPosition();
            });
        }

        if (this.zipCodeInput) {
            this._initAutocomplete();
        }
    }

    getCurrentPosition() {

        this.#showLoaderAnimation();

        let pos = null;
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition((position) => {
                pos = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };

                this.#triggerRefresh(pos, pos);

            }, () => {
                this.#removeCookies();
                this.#hideLoaderAnimation();
            });
        }else  {
            console.info('browser does not support geolocation');
            this.#removeCookies();
            this.#hideLoaderAnimation();
        }
    }

    _initAutocomplete() {
        this.loader.load()
            .then((google) => {
                this.autocomplete = new google.maps.places.Autocomplete(
                    this.zipCodeInput,
                    {
                        types: ['postal_code', 'locality'],
                        componentRestrictions: {
                            'country': ['BE', 'NL']
                        },
                        fields: ['name', 'geometry']
                    }
                );
                this.autocomplete.addListener('place_changed', (event) => {
                    this.#triggerRefresh(
                        this.#coordinatesToString(this.autocomplete.getPlace().geometry.location),
                        this.autocomplete.gm_accessors_.place.Am.formattedPrediction
                    )
                })
            });
    }

    async #setPlaceCookie(coordinates) {
        const geocoder = new google.maps.Geocoder();
        const response = await geocoder.geocode({ location: coordinates });
        Cookies.set('place', this.#findLocationName(response), { sameSite: 'strict' });
    }

    #findLocationName(response) {
        const filteredResults = response.results.filter((item) => {
            return item.types.includes('postal_code');
        })

        return filteredResults[0].formatted_address;
    }

    #coordinatesToString(position) {
        if (typeof position.lat === 'function') {
            return `${position.lat()}, ${position.lng()}`;
        }

        return `${position.lat}, ${position.lng}`;
    }

    #showLoaderAnimation() {
        const wrapperWidth = this.listingWrapper.offsetWidth;
        const wrapperHeight = this.listingWrapper.offsetHeight;
        if(this.loaderAnnimation) {
            this.loaderAnnimation.classList.remove('d-none');
            this.listingWrapper.classList.add('d-none');
            this.loaderAnnimation.style.width = `${wrapperWidth}px`;
            this.loaderAnnimation.style.height = `${wrapperHeight}px`;
        }
    }

    #hideLoaderAnimation() {
        this.loaderAnnimation.classList.add('d-none');
        this.listingWrapper.classList.remove('d-none');
    }

    #removeCookies() {
        Cookies.remove('place');
        Cookies.remove('location');
    }

    async #triggerRefresh(location, place) {
        if (typeof location === 'object') {
            Cookies.set('location', this.#coordinatesToString(location), { sameSite: 'strict' });
        } else {
            Cookies.set('location', location, { sameSite: 'strict' });
        }

        if (typeof place === 'object') {
            await this.#setPlaceCookie(place);
        } else {
            Cookies.set('place', place, { sameSite: 'strict' });
        }

        this.options.sorting = 'location';
        this.listing.changeListing();
        this.#hideLoaderAnimation();
    }
}