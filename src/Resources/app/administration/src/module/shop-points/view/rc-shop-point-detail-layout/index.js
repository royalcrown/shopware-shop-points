import template from './rc-shop-point-detail-layout.html.twig';

const { Component, State, Context, Utils } = Shopware;
const { Criteria } = Shopware.Data;
const { mapState, mapGetters } = Component.getComponentHelper();
const { cloneDeep, merge, get } = Utils.object;

Component.register('shop-point-detail-layout', {
    template,

    inject: ['repositoryFactory', 'cmsService', 'feature', 'acl'],

    data() {
        return {
            showLayoutModal: false,
            isConfigLoading: false,
        };
    },

    computed: {
        cmsPageRepository() {
            return this.repositoryFactory.create('cms_page');
        },

        cmsPageId() {
            return get(this.shopPoint, 'cmsPageId', null);
        },

        showCmsForm() {
            return (!this.isLoading || !this.isConfigLoading) && !this.currentPage.locked;
        },

        ...mapState('rcShopPointDetail', [
            'shopPoint',
        ]),

        ...mapGetters('rcShopPointDetail', [
            'isLoading',
        ]),

        ...mapState('cmsPageState', [
            'currentPage',
        ]),

        cmsPageCriteria() {
            const criteria = new Criteria(1, 25);
            criteria.addAssociation('previewMedia');
            criteria.addAssociation('sections');
            criteria.getAssociation('sections').addSorting(Criteria.sort('position'));

            criteria.addAssociation('sections.blocks');
            criteria.getAssociation('sections.blocks')
                .addSorting(Criteria.sort('position', 'ASC'))
                .addAssociation('slots');

            return criteria;
        },
    },

    watch: {
        cmsPageId() {
            State.dispatch('cmsPageState/resetCmsPageState');
            this.handleGetCmsPage();
        },

        shopPoint: {
            deep: true,
            handler(value) {
                if (!value) {
                    return;
                }

                this.updateCmsPageDataMapping();
                this.handleGetCmsPage();
            },
        },
    },

    created() {
        this.createdComponent();
    },

    methods: {
        createdComponent() {
            // Keep current layout configuration if page sections exist
            const sections = this.currentPage?.sections ?? [];

            if (sections.length) {
                return;
            }

            this.handleGetCmsPage();
        },

        onOpenLayoutModal() {
            this.showLayoutModal = true;
        },

        onCloseLayoutModal() {
            this.showLayoutModal = false;
        },

        onOpenInPageBuilder() {
            if (!this.currentPage) {
                this.$router.push({ name: 'sw.cms.create' });
            } else {
                this.$router.push({ name: 'sw.cms.detail', params: { id: this.currentPage.id } });
            }
        },

        onSelectLayout(cmsPageId) {
            if (!this.shopPoint) {
                return;
            }

            this.shopPoint.cmsPageId = cmsPageId;
            this.shopPoint.slotConfig = null;
            State.commit('rcShopPointDetail/setShopPoint', this.shopPoint);
        },

        handleGetCmsPage() {
            if (!this.cmsPageId) {
                return;
            }

            this.isConfigLoading = true;

            this.cmsPageRepository.get(this.cmsPageId, Context.api, this.cmsPageCriteria).then((cmsPage) => {
                if (this.shopPoint.slotConfig && cmsPage) {
                    cmsPage.sections.forEach((section) => {
                        section.blocks.forEach((block) => {
                            block.slots.forEach((slot) => {
                                if (!this.shopPoint.slotConfig[slot.id]) {
                                    return;
                                }

                                slot.config = slot.config || {};
                                merge(slot.config, cloneDeep(this.shopPoint.slotConfig[slot.id]));
                            });
                        });
                    });
                }

                State.commit('cmsPageState/setCurrentPage', cmsPage);
                this.updateCmsPageDataMapping();
                this.isConfigLoading = false;
            });
        },

        updateCmsPageDataMapping() {
            Shopware.State.commit('cmsPageState/setCurrentMappingEntity', 'shopPoint');
            Shopware.State.commit(
                'cmsPageState/setCurrentMappingTypes',
                this.cmsService.getEntityMappingTypes('shopPoint'),
            );
            Shopware.State.commit('cmsPageState/setCurrentDemoEntity', this.shopPoint);
        },

        onResetLayout() {
            this.onSelectLayout(null);
        },
    },
});
