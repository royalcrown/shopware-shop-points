import template from './rc-shop-point-detail-general.html.twig';

const { Criteria } = Shopware.Data;


const { Component, Context, Utils, Mixin } = Shopware;
const { mapState, mapGetters } = Component.getComponentHelper();
const { isEmpty } = Utils.types;

Component.register('shop-point-detail-general', {
    template,

    inject: ['repositoryFactory', 'acl'],

    mixins: [
        Mixin.getByName('notification'),
    ],

    props: {
        shopPointId: {
            type: String,
            required: false,
            default: null,
        },
    },

    data() {
        return {
            showMediaModal: false,
            mediaDefaultFolderId: null,
        };
    },

    computed: {
        ...mapState('rcShopPointDetail', [
            'shopPoint',
            'loading',
        ]),

        ...mapGetters('rcShopPointDetail', [
            'isLoading',
        ]),

        ...mapState('rcShopPointDetail', {
        }),
        shopPointMediaRepository() {
            return this.repositoryFactory.create(this.shopPoint.mediaList.entity);
        },
    },

    watch: {
    },

    created() {

    },

    methods: {
        onOpenMediaModal() {
            this.showMediaModal = true;
        },

        onCloseMediaModal() {
            this.showMediaModal = false;
        },

        onOpenDownloadMediaModal() {
            this.showDownloadMediaModal = true;
        },

        onCloseDownloadMediaModal() {
            this.showDownloadMediaModal = false;
        },

        onAddMedia(media) {
            if (isEmpty(media)) {
                return;
            }

            media.forEach((item) => {
                this.addMedia(item).catch(({ fileName }) => {
                    this.createNotificationError({
                        message: this.$tc('sw-product.mediaForm.errorMediaItemDuplicated', 0, { fileName }),
                    });
                });
            });
        },

        addMedia(media) {
            if (this.isExistingMedia(media)) {
                return Promise.reject(media);
            }

            const newMedia = this.shopPointMediaRepository.create(Context.api);
            newMedia.mediaId = media.id;
            newMedia.media = {
                url: media.url,
                id: media.id,
            };

            if (isEmpty(this.shopPoint.mediaList)) {
                this.setMediaAsCover(newMedia);
            }

            this.shopPoint.mediaList.add(newMedia);

            return Promise.resolve();
        },

        isExistingMedia(media) {
            return this.shopPoint.mediaList.some(({ id, mediaId }) => {
                return id === media.id || mediaId === media.id;
            });
        },

        setMediaAsCover(media) {
            media.position = 0;
            this.shopPoint.coverId = media.id;
        },

    },
});


