import template from './rc-shop-detail-address.html.twig';

const { Component, Utils, Mixin } = Shopware;
const { mapState, mapGetters } = Component.getComponentHelper();

Component.register('shop-point-detail-address', {
    template,

    inject: ['repositoryFactory', 'acl'],

    mixins: [
        Mixin.getByName('notification'),
    ],

    props: {
        shopPointId: {
            type: String,
            required: false,
            default: null,
        },
    },

    data() {
        return {
            showMediaModal: false,
            mediaDefaultFolderId: null,
        };
    },

    computed: {
        ...mapState('rcShopPointDetail', [
            'shopPoint',
            'loading',
        ]),

        ...mapGetters('rcShopPointDetail', [
            'isLoading',
        ]),

        ...mapState('rcShopPointDetail', {
        }),
    },

    watch: {
    },

    created() {

    },

    methods: {

    },
});


