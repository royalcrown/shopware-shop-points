import nlBE from './snippet/nl-BE.json';
import enGB from './snippet/en-GB.json';

import './page/rc-shop-point-list';
import './page/rc-shop-point-detail';
import './view/rc-shop-point-detail-general';
import './view/rc-shop-point-detail-address';
import './view/rc-shop-point-detail-opening-hours';
import './view/rc-shop-point-detail-layout';
import './view/rc-shop-point-detail-media';
import './component/rc-shop-point-base-form';
import './component/rc-shop-point-layout-assignment';
import './component/rc-shop-point-opening-hours-form';
import './component/rc-shop-point-address-form';
import './component/rc-shop-point-media-form';

Shopware.Module.register('shop-points', {
    type: 'plugin',
    name: 'shop-points',
    title: 'rc-shop-point.general.mainMenuItemGeneral',
    description: 'rc-shop-point.general.descriptionTextModule',
    color: '#ff68b4',
    icon: 'regular-content',
    entity: 'rc_shop_point',
    snippets: {
        'nl-BE': nlBE,
        'en-GB': enGB
    },
    navigation: [{
        id: 'shop-points',
        label: 'rc-shop-point.general.shopPoints',
        color: '#ff68b4',
        path: 'shop.points.list',
        icon: 'regular-content',
        parent: 'sw-content',
        position: 1
    }],
    routes: {
        list: {
            component: 'shop-point-list',
            path: 'list',
            meta: {
                appSystem: {
                    view: 'list',
                },
            },
        },
        create: {
            component: 'shop-point-detail',
            path: 'create',
            props: {
                default: (route) => ({ creationStates: route.query.creationStates ?? ['is-physical'] }),
            },
            redirect: {
                name: 'shop.points.create.general',
            },
            children: {
                general: {
                    component: 'shop-point-detail-general',
                    path: 'general',
                    meta: {
                        parentPath: 'shop.points.list',
                    },
                },
            },
        },
        detail: {
            component: 'shop-point-detail',
            path: 'detail/:id?',
            props: {
                default(route) {
                    return { shopPointId: route.params.id };
                },
            },
            redirect: {
                name: 'shop.points.detail.general',
            },
            meta: {
                parentPath: 'shop.points.list',
                appSystem: {
                    view: 'detail',
                },
            },
            children: {
                general: {
                    component: 'shop-point-detail-general',
                    path: 'general',
                    meta: {
                        parentPath: 'shop.points.list',
                        privilege: 'product.viewer',
                    },
                },
                media: {
                    component: 'shop-point-detail-media',
                    path: 'media',
                    meta: {
                        parentPath: 'shop.points.list',
                        privilege: 'product.viewer',
                    },
                },
                address: {
                    component: 'shop-point-detail-address',
                    path: 'address',
                    meta: {
                        parentPath: 'shop.points.list',
                        privilege: 'product.viewer',
                    },
                },
                openingHours: {
                    component: 'shop-point-detail-opening-hours',
                    path: 'opening-hours',
                    meta: {
                        parentPath: 'shop.points.list',
                        privilege: 'product.viewer',
                    },
                },
                layout: {
                    component: 'shop-point-detail-layout',
                    path: 'layout',
                    meta: {
                        parentPath: 'shop.points.list',
                        privilege: 'product.viewer',
                    },
                },
            },
        }
    },
});
