import template from './rc-shop-point-base-form.html.twig';

const { Component, Mixin } = Shopware;
const { mapState, mapGetters } = Component.getComponentHelper();

Component.register('shop-point-base-form', {
    template,

    inject: ['repositoryFactory', 'acl'],

    mixins: [
        Mixin.getByName('notification'),
        Mixin.getByName('placeholder'),
    ],

    computed: {
        ...mapState('rcShopPointDetail', [
            'shopPoint',
            'loading',
        ]),
        ...mapGetters('rcShopPointDetail', [
            'isLoading',
        ]),

        // mediaItem() {
        //     return this.shopPoint !== null ? this.shopPoint.media : null;
        // },
        //
        // mediaRepository() {
        //     return this.repositoryFactory.create('media');
        // },
    },
    data() {
        return {
            showMediaModal: false,
        };
    },

    watch: {
    },

    created() {
    },

    methods: {
        nameRequired() {},
        // onMediaSelectionChange(mediaItems) {
        //     const media = mediaItems[0];
        //     if (!media) {
        //         return;
        //     }
        //
        //     this.mediaRepository.get(media.id).then((updatedMedia) => {
        //         this.shopPoint.mediaId = updatedMedia.id;
        //         this.shopPoint.media = updatedMedia;
        //     });
        // },
        //
        // onSetMediaItem({ targetId }) {
        //     this.mediaRepository.get(targetId).then((updatedMedia) => {
        //         this.shopPoint.mediaId = targetId;
        //         this.shopPoint.media = updatedMedia;
        //     });
        // },
        //
        // onRemoveMediaItem() {
        //     this.shopPoint.mediaId = null;
        //     this.shopPoint.media = null;
        // },
        //
        // onMediaDropped(dropItem) {
        //     // to be consistent refetch entity with repository
        //     this.onSetMediaItem({ targetId: dropItem.id });
        // },
    },
});