import template from './rc-shop-point-opening-hours-form.html.twig';
import './rc-shop-point-opening-hours-form.scss';

const { Component, Mixin } = Shopware;
const { mapState, mapGetters } = Component.getComponentHelper();

Component.register('shop-point-opening-hours-form', {
    template,

    inject: ['repositoryFactory', 'acl'],

    mixins: [
        Mixin.getByName('notification'),
        Mixin.getByName('placeholder'),
    ],

    computed: {
        ...mapState('rcShopPointDetail', [
            'shopPoint',
            'loading',
        ]),
        ...mapGetters('rcShopPointDetail', [
            'isLoading',
        ]),
        openingHourRepository() {
            return this.repositoryFactory.create('rc_opening_hour');
        },
    },
    data() {
        return {
            showMediaModal: false,
            days: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'],
        };
    },
    methods: {
        addHour(suffix) {
            const propertyName = 'openingHours' + suffix;
            this.shopPoint[propertyName].hours.add(
                this.openingHourRepository.create()
            );
        },
        removeHour(suffix,  hourToRemove) {
            const propertyName = 'openingHours' + suffix;
            const index = this.shopPoint[propertyName].hours.indexOf(hourToRemove);
            if (index >= 0) {
                this.shopPoint[propertyName].hours.splice(index, 1)
            }
        }
    },
});