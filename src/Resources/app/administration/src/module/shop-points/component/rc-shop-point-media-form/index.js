/*
 * @package inventory
 */

import template from './rc-shop-point-media-form.html.twig';
import './rc-shop-point-media-form.scss';

const { Component, Mixin } = Shopware;
const { mapState, mapGetters } = Component.getComponentHelper();

Shopware.Component.register('shop-point-media-form', {
    
    template,

    inject: ['repositoryFactory', 'acl'],

    mixins: [
        Mixin.getByName('notification'),
    ],

    props: {
        disabled: {
            type: Boolean,
            required: false,
            default: false,
        },

        isInherited: {
            type: Boolean,
            required: false,
            default: false,
        },
    },

    data() {
        return {
            showCoverLabel: true,
            isMediaLoading: false,
            columnCount: 5,
            columnWidth: 90,
        };
    },

    computed: {
        ...mapState('rcShopPointDetail', [
            'shopPoint',
            'loading',
        ]),
        ...mapGetters('rcShopPointDetail', [
            'isLoading',
        ]),

        mediaItems() {
            const mediaItems = this.shopPointMedia.slice();
            const placeholderCount = this.getPlaceholderCount(this.columnCount);

            if (placeholderCount === 0) {
                return mediaItems;
            }

            for (let i = 0; i < placeholderCount; i += 1) {
                mediaItems.push(this.createPlaceholderMedia(mediaItems));
            }
            return mediaItems;
        },

        cover() {
            if (!this.shopPoint) {
                return null;
            }
            const coverId = this.shopPoint.cover ? this.shopPoint.cover.mediaId : this.shopPoint.coverId;
            return this.shopPoint.mediaList.find(media => media.id === coverId);
        },


        shopPointMediaRepository() {
            return this.repositoryFactory.create('rc_shop_point_media');
        },

        shopPointMedia() {
            if (!this.shopPoint) {
                return [];
            }
            return this.shopPoint.mediaList;
        },

        shopPointMediaStore() {
            return this.shopPoint.getAssociation('mediaList');
        },

        gridAutoRows() {
            return `grid-auto-rows: ${this.columnWidth}`;
        },

        currentCoverID() {
            const coverMediaItem = this.shopPointMedia.find(coverMedium => coverMedium.media.id === this.shopPoint.coverId);

            return coverMediaItem.id;
        },
    },

    methods: {
        onOpenMedia() {
            this.$emit('media-open');
        },

        updateColumnCount() {
            this.$nextTick(() => {
                if (this.isLoading) {
                    return false;
                }

                const cssColumns = window.getComputedStyle(this.$refs.grid, null)
                    .getPropertyValue('grid-template-columns')
                    .split(' ');
                this.columnCount = cssColumns.length;
                this.columnWidth = cssColumns[0];

                return true;
            });
        },

        getPlaceholderCount(columnCount) {
            if (this.shopPointMedia.length + 3 < columnCount * 2) {
                columnCount *= 2;
            }

            let placeholderCount = columnCount;

            if (this.shopPointMedia.length !== 0) {
                placeholderCount = columnCount - ((this.shopPointMedia.length) % columnCount);
                if (placeholderCount === columnCount) {
                    return 0;
                }
            }

            return placeholderCount;
        },

        createPlaceholderMedia(mediaItems) {
            return {
                isPlaceholder: true,
                isCover: mediaItems.length === 0,
                media: {
                    isPlaceholder: true,
                    name: '',
                },
                mediaId: mediaItems.length.toString(),
            };
        },

        buildShopPointMedia(mediaId) {
            this.isLoading = true;

            const shopPointMedia = this.shopPointMediaStore.create();
            shopPointMedia.mediaId = mediaId;

            if (this.shopPointMedia.length === 0) {
                shopPointMedia.position = 0;
                this.shopPoint.cover = shopPointMedia;
                this.shopPoint.coverId = shopPointMedia.id;
            } else {
                shopPointMedia.position = this.shopPointMedia.length + 1;
            }
            this.isLoading = false;

            return shopPointMedia;
        },

        successfulUpload({ targetId }) {
            // on replace
            if (this.shopPoint.mediaList.find((shopPointMedia) => shopPointMedia.mediaId === targetId)) {
                return;
            }

            const shopPointMedia = this.createMediaAssociation(targetId);
            this.shopPoint.mediaList.add(shopPointMedia);
        },

        createMediaAssociation(targetId) {
            const shopPointMedia = this.shopPointMediaRepository.create();

            shopPointMedia.shopPointId = this.shopPoint.id;
            shopPointMedia.mediaId = targetId;

            if (this.shopPoint.mediaList.length <= 0) {
                shopPointMedia.position = 0;
                this.shopPoint.coverId = shopPointMedia.id;
            } else {
                shopPointMedia.position = this.shopPoint.mediaList.length;
            }
            return shopPointMedia;
        },

        onUploadFailed(uploadTask) {
            const toRemove = this.shopPoint.mediaList.find((shopPointMedia) => {
                return shopPointMedia.mediaId === uploadTask.targetId;
            });
            if (toRemove) {
                if (this.shopPoint.coverId === toRemove.id) {
                    this.shopPoint.coverId = null;
                }
                this.shopPoint.mediaList.remove(toRemove.id);
            }
            this.isLoading = false;
        },

        removeCover() {
            this.shopPoint.cover = null;
            this.shopPoint.coverId = null;
        },

        isCover(shopPointMedia) {
            const coverId = this.shopPoint.cover ? this.shopPoint.cover.id : this.shopPoint.coverId;

            if (this.shopPoint.mediaList.length === 0 || shopPointMedia.isPlaceholder) {
                return false;
            }

            return shopPointMedia.id === coverId;
        },

        removeFile(shopPointMedia) {

            // remove cover id if mediaId matches
            if (this.shopPoint.coverId === shopPointMedia.id) {
                this.shopPoint.cover = null;
                this.shopPoint.coverId = null;
            }

            if (this.shopPoint.coverId === null && this.shopPoint.mediaList.length > 0) {
                this.shopPoint.coverId = this.shopPoint.mediaList.first().id;
            }

            this.shopPoint.mediaList.remove(shopPointMedia.id);
        },

        markMediaAsCover(shopPointMedia) {
            this.shopPoint.cover = shopPointMedia;
            this.shopPoint.coverId = shopPointMedia.id;

            this.shopPoint.mediaList.moveItem(shopPointMedia.position, 0);
            this.updateMediaItemPositions();
        },

        onDropMedia(dragData) {
            if (this.shopPoint.mediaList.find((ShopPointMedia) => ShopPointMedia.mediaId === dragData.id)) {
                return;
            }

            const shopPointMedia = this.createMediaAssociation(dragData.mediaItem.id);
            if (this.shopPoint.mediaList.length === 0) {
                // set media item as cover
                shopPointMedia.position = 0;
                this.shopPoint.cover = shopPointMedia;
                this.shopPoint.coverId = shopPointMedia.id;
            }

            this.shopPoint.mediaList.add(shopPointMedia);
        },

        onMediaItemDragSort(dragData, dropData, validDrop) {
            if (validDrop !== true
                || (dragData.id === this.shopPoint.coverId && dragData.position === 0)
                || (dropData.id === this.shopPoint.coverId && dropData.position === 0)) {
                return;
            }


            this.shopPoint.mediaList.moveItem(dragData.position, dropData.position);

            this.updateMediaItemPositions();
        },

        updateMediaItemPositions() {
            this.shopPointMedia.forEach((medium, index) => {
                medium.position = index;
            });
        },
    }
});
