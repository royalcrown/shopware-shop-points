import template from './rc-shop-point-layout-assignment.html.twig';

import './rc-shop-point-layout-assignment.scss';

Shopware.Component.register('shop-point-layout-assignment', {
    template,

    inject: ['acl'],

    props: {
        cmsPage: {
            type: Object,
            required: false,
            default: null,
        },
    },

    methods: {
        openLayoutModal() {
            this.$emit('modal-layout-open');
        },

        openInPageBuilder() {
            this.$emit('button-edit-click');
        },

        onLayoutReset() {
            this.$emit('button-delete-click');
        },
    },
});
