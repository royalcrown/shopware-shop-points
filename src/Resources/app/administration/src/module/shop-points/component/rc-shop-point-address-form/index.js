import template from './rc-shop-point-address-form.html.twig';
import './rc-shop-point-address-form.scss';

const { Component, Mixin } = Shopware;
const { mapState, mapGetters } = Component.getComponentHelper();

Component.register('shop-point-address-form', {
    template,

    inject: ['repositoryFactory', 'acl'],

    mixins: [
        Mixin.getByName('notification'),
        Mixin.getByName('placeholder'),
    ],

    computed: {
        ...mapState('rcShopPointDetail', [
            'shopPoint',
            'loading',
        ]),
        ...mapGetters('rcShopPointDetail', [
            'isLoading',
        ]),

        mediaItem() {
            return this.shopPoint !== null ? this.shopPoint.media : null;
        },

        mediaRepository() {
            return this.repositoryFactory.create('media');
        },
    },
    data() {
        return {
            showMediaModal: false,
        };
    },

    methods: {
        nameRequired() {},
        addHour(suffix) {
            const propertyName = 'openingHours' + suffix;
            this.shopPoint[propertyName].hours.add(
                this.openingHourRepository.create()
            );
        },
        removeHour(suffix,  hourToRemove) {
            const propertyName = 'openingHours' + suffix;
            const index = this.shopPoint[propertyName].hours.indexOf(hourToRemove);
            if (index >= 0) {
                this.shopPoint[propertyName].hours.splice(index, 1)
            }
        }
    },
});