export default {
    namespaced: true,

    state() {
        return {
            shopPoint: {},
            apiContext: {},
            loading: {
                init: false,
                shopPoint: false,
            },
        };
    },

    getters: {
        isLoading: (state) => {
            return Object.values(state.loading).some((loadState) => loadState);
        },
    },

    mutations: {
        setApiContext(state, apiContext) {
            state.apiContext = apiContext;
        },

        setLoading(state, value) {
            const name = value[0];
            const data = value[1];

            if (typeof data !== 'boolean') {
                return false;
            }

            if (state.loading[name] !== undefined) {
                state.loading[name] = data;
                return true;
            }
            return false;
        },

        setShopPointId(state, shopPointId) {
            state.shopPointId = shopPointId;
        },

        setShopPoint(state, newShopPoint) {
            state.shopPoint = newShopPoint;
        },
    },
};
