import template from './rc-shop-point-detail.html.twig';
import rcShopPointState from './state';

const { Mixin, Context } = Shopware;
const { mapState, mapGetters } = Shopware.Component.getComponentHelper();
const { Criteria, ChangesetGenerator } = Shopware.Data;
const { cloneDeep } = Shopware.Utils.object;
const type = Shopware.Utils.types;


Shopware.Component.register('shop-point-detail', {

    template,

    inject: [
        'repositoryFactory',
        'acl',
        'systemConfigApiService',
        'entityValidationService',
    ],

    mixins: [
        Mixin.getByName('placeholder'),
        Mixin.getByName('notification'),
    ],

    shortcuts: {
        'SYSTEMKEY+S': {
            active() {
                return true;
            },
            method: 'onSave',
        },
        ESCAPE: 'onCancel',
    },

    props: {
        shopPointId: {
            type: String,
            required: false,
            default: null,
        },
    },

    data() {
        return {
            isSaveSuccessful: false,
            days: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'],
        };
    },
    computed: {
        ...mapState('rcShopPointDetail', [
            'shopPoint',
        ]),
        ...mapGetters('rcShopPointDetail', [
            'isLoading',
        ]),
        ...mapState('cmsPageState', [
            'currentPage',
        ]),
        shopPointRepository() {
            return this.repositoryFactory.create('rc_shop_point');
        },
        openingHoursRepository() {
            return this.repositoryFactory.create('rc_opening_hours');
        },
        tooltipSave() {
            const systemKey = this.$device.getSystemKey();

            return {
                message: `${systemKey} + S`,
                appearance: 'light',
            };
        },

        tooltipCancel() {
            return {
                message: 'ESC',
                appearance: 'light',
            };
        },

        isSystemLanguage() {
            return this.languageId === Context.api.systemLanguageId;
        },

        // nameRequired() {
        //     return this.isSystemLanguage;
        // },

        shopPointCriteria() {
            const criteria = new Criteria(1, 25);

            criteria
                .addAssociation('cmsPage')
                .addAssociation('cover')
                .addAssociation('mediaList')
            ;

            this.days.forEach((day) => {
                const propertyName = 'openingHours' + day;
                const propertyNameHours = 'openingHours' + day +'.hours';
                criteria.addAssociation(propertyName);
                criteria.addAssociation(propertyNameHours);
            })

            return criteria;
        },
    },

    watch: {
        shopPointId() {
            this.createdComponent();
        },
    },

    beforeCreate() {
        Shopware.State.registerModule('rcShopPointDetail', rcShopPointState);
    },

    created() {
        this.createdComponent();
    },

    beforeDestroy() {
        Shopware.State.unregisterModule('rcShopPointDetail');
    },

    methods: {
        createdComponent() {
            Shopware.ExtensionAPI.publishData({
                id: 'rc-shop-point-detail__shopPoint',
                path: 'shopPoint',
                scope: this,
            });

            Shopware.ExtensionAPI.publishData({
                id: 'rc-shop-point-detail__cmsPage',
                path: 'currentPage',
                scope: this,
            });

            Shopware.State.dispatch('cmsPageState/resetCmsPageState');

            this.languageId = Context.api.languageId;

            // initialize default state
            this.initState();
        },

        initState() {
            Shopware.State.commit('rcShopPointDetail/setApiContext', Shopware.Context.api);

            if (this.shopPointId) {
                return this.loadState();
            }

            return this.createState();
        },

        loadState() {
            Shopware.State.commit('rcShopPointDetail/setShopPointId', this.shopPointId);

            this.loadShopPoint();
        },
        createState() {
            Shopware.State.commit('rcShopPointDetail/setLoading', ['shopPoint', true]);
            Shopware.State.commit('rcShopPointDetail/setShopPoint', this.createShopPoint());
            Shopware.State.commit('rcShopPointDetail/setShopPointId', this.shopPoint.id);
            Shopware.State.commit('rcShopPointDetail/setLoading', ['shopPoint', false]);
        },
        loadShopPoint() {
            Shopware.State.commit('rcShopPointDetail/setLoading', ['shopPoint', true]);

            return this.shopPointRepository.get(
                this.shopPointId,
                Shopware.Context.api,
                this.shopPointCriteria,
            ).then((shopPoint) => {
                this.addMissingOpeningHours(shopPoint);
                Shopware.State.commit('rcShopPointDetail/setShopPoint', shopPoint);
                // Shopware.State.commit('rcShopPointDetail/setShopPointId', shopPoint.id);
                Shopware.State.commit('rcShopPointDetail/setLoading', ['shopPoint', false]);
            });
        },

        createShopPoint() {
            if (!Shopware.State.getters['context/isSystemDefaultLanguage']) {
                Shopware.State.commit('context/resetLanguageToDefault');
            }

            const shopPoint = this.shopPointRepository.create(Shopware.Context.api);

            this.addMissingOpeningHours(shopPoint);

            return shopPoint;
        },

        addMissingOpeningHours(shopPoint) {

            this.days.forEach((day) => {
                const propertyName = 'openingHours' + day;
                if (!shopPoint[propertyName]) {
                    shopPoint[propertyName] = this.openingHoursRepository.create();
                }
            })
        },

        abortOnLanguageChange() {
            return this.shopPointRepository.hasChanges(this.shopPoint);
        },

        saveOnLanguageChange() {
            return this.onSave();
        },

        onChangeLanguage(languageId) {
            Shopware.State.commit('context/setApiLanguageId', languageId);
            this.initState();
        },

        saveFinish() {
            this.isSaveSuccessful = false;
            Shopware.State.commit('rcShopPointDetail/setLoading', ['shopPoint', false]);

            if (!this.shopPointId) {
                this.$router.push({ name: 'shop.points.detail', params: { id: this.shopPoint.id } });
            }
        },

        onSave() {
            this.isSaveSuccessful = false;
            Shopware.State.commit('rcShopPointDetail/setLoading', ['shopPoint', true]);

            const pageOverrides = this.getCmsPageOverrides();

            if (type.isPlainObject(pageOverrides)) {
                this.shopPoint.slotConfig = cloneDeep(pageOverrides);
            }

            return this.saveShopPoint().then(this.saveFinish);
        },

        showErrorNotification() {
            this.createNotificationError({
                message: this.$tc(
                    'global.notification.notificationSaveErrorMessageRequiredFieldsInvalid',
                ),
            });
        },

        saveShopPoint() {
            return this.shopPointRepository.save(this.shopPoint);
        },

        onCancel() {
            this.$router.push({ name: 'shop.points.list' });
        },
        getCmsPageOverrides() {
            if (this.currentPage === null) {
                return null;
            }

            this.deleteSpecifcKeys(this.currentPage.sections);

            const changesetGenerator = new ChangesetGenerator();
            const { changes } = changesetGenerator.generate(this.currentPage);

            const slotOverrides = {};
            if (changes === null) {
                return slotOverrides;
            }

            if (type.isArray(changes.sections)) {
                changes.sections.forEach((section) => {
                    if (type.isArray(section.blocks)) {
                        section.blocks.forEach((block) => {
                            if (type.isArray(block.slots)) {
                                block.slots.forEach((slot) => {
                                    slotOverrides[slot.id] = slot.config;
                                });
                            }
                        });
                    }
                });
            }

            return slotOverrides;
        },


        deleteSpecifcKeys(sections) {
            if (!sections) {
                return;
            }

            sections.forEach((section) => {
                if (!section.blocks) {
                    return;
                }

                section.blocks.forEach((block) => {
                    if (!block.slots) {
                        return;
                    }

                    block.slots.forEach((slot) => {
                        if (!slot.config) {
                            return;
                        }

                        Object.values(slot.config).forEach((configField) => {
                            if (configField.entity) {
                                delete configField.entity;
                            }
                            if (configField.hasOwnProperty('required')) {
                                delete configField.required;
                            }
                            if (configField.type) {
                                delete configField.type;
                            }
                        });
                    });
                });
            });
        },
    },
});
