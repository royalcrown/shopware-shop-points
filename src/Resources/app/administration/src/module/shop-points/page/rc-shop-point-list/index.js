import template from './rc-shop-point-list.html.twig';

const { Mixin } = Shopware;
const { Criteria } = Shopware.Data;


Shopware.Component.register('shop-point-list', {
    template,

    inject: [
        'repositoryFactory',
        'acl'
    ],

    mixins: [
        Mixin.getByName('listing'),
        Mixin.getByName('notification'),
    ],

    data() {
        return {
            shopPoints: null,
            isLoading: false,
            sortBy: 'name',
            sortDirection: 'DESC',
            naturalSorting: false,
            total: 0,
        }
    },

    computed: {
        shopPointRepository() {
            const options = {
                version: 1 // default is the latest api version
            };
            return this.repositoryFactory.create('rc_shop_point', null, options);
        },
    },

    methods: {
        onChangeLanguage() {
            return this.getList();
        },
        getShopPointColumns() {
            return [{
                property: 'name',
                dataIndex: 'name',
                inlineEdit: 'string',
                label: 'rc-shop-point.list.columnName',
                routerLink: 'shop.points.detail',
                width: '250px',
                allowResize: true,
                primary: true,
            }, {
                property: 'description',
                label: 'rc-shop-point.list.columnDescription',
                width: '250px',
                allowResize: true,
            }, {
                property: 'coordinates',
                label: 'rc-shop-point.list.columnCoordinates',
                width: '250px',
                allowResize: true,
            }, {
                property: 'updatedAt',
                label: 'rc-shop-point.list.columnDateUpdated',
                align: 'right',
                allowResize: true,
            }, {
                property: 'active',
                label: 'rc-shop-point.list.columnActive',
                allowResize: true,
            }];
        },
        onDeleteItemFailed({ id, errorResponse }) {
            const stream = this.shopPoints?.get(id);
            const message = errorResponse?.response?.data?.errors?.[0]?.detail || null;

            if (!stream) {
                return;
            }

            this.createNotificationError({ message });
        },

        onDeleteItemsFailed({ selectedIds, errorResponse }) {
            selectedIds.forEach((id) => {
                this.onDeleteItemFailed({ id, errorResponse });
            });
        },
        async getList() {
            this.isLoading = true;

            let criteria = new Criteria(this.page, this.limit);

            criteria.setTerm(this.term);

            this.naturalSorting = this.sortBy === 'name';
            criteria.addSorting(Criteria.sort(this.sortBy, this.sortDirection, this.naturalSorting));

            return this.shopPointRepository.search(criteria).then((items) => {
                this.total = items.total;
                this.shopPoints = items;
                this.isLoading = false;

                return items;
            }).catch((e) => {
                this.isLoading = false;
            });
        },
    }
});
