// import Criteria from 'src/core/data/criteria.data';
import template from './sw-cms-el-config-shop-point-map.html.twig';
import './sw-cms-el-config-shop-point-map.scss';

const { Mixin } = Shopware;

Shopware.Component.register('sw-cms-el-config-shop-point-map', {
    template,

    inject: ['repositoryFactory'],

    mixins: [
        Mixin.getByName('cms-element'),
    ],

    // computed: {
    //     productRepository() {
    //         return this.repositoryFactory.create('product');
    //     },
    //
    //     productSelectContext() {
    //         const context = { ...Shopware.Context.api };
    //         context.inheritance = true;
    //
    //         return context;
    //     },
    //
    //     productCriteria() {
    //         const criteria = new Criteria(1, 25);
    //         criteria.addAssociation('options.group');
    //
    //         return criteria;
    //     },
    // },
    //
    created() {
        this.createdComponent();
    },

    methods: {
        createdComponent() {
            this.initElementConfig('shop-point-map');
        },
        onChangeMapType(type) {
            this.element.config.mapType.value = type;
            this.$emit('element-update', this.element);
        },
        onChangeZoom(zoom) {
            if (zoom !== null) {
                this.element.config.zoom.value = zoom;
            } else {
                this.element.config.zoom.value = 8;
            }
            this.$emit('element-update', this.element);
        },
        onChangeCenter(center) {
            if (center !== null) {
                this.element.config.center.value = center;
            } else {
                this.element.config.center.value = '50.84895206110133, 4.357618657122824';
            }
            this.$emit('element-update', this.element);
        },
        onChangeHeight(value) {
            this.element.config.height.value = value;
            this.$emit('element-update', this.element);
        }

    },
});
