import template from './sw-cms-el-preview-shop-point-listing.html.twig';
import './sw-cms-el-preview-shop-point-listing.scss';

Shopware.Component.register('sw-cms-el-preview-shop-point-listing', {
    template
});
