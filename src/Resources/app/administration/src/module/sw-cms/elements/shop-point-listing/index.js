import './preview';
import './component';
// import './config';

Shopware.Service('cmsService').registerCmsElement({
    name: 'shop-point-listing',
        label: 'sw-cms.blocks.commerce.shopPointListing.label',
    // hidden: true,
    // removable: false,
    component: 'sw-cms-el-shop-point-listing',
    previewComponent: 'sw-cms-el-preview-shop-point-listing',
    // configComponent: 'sw-cms-el-config-shop-point-listing',
    defaultConfig: {
        boxLayout: {
            source: 'static',
            value: 'standard',
        },
        showSorting: {
            source: 'static',
            value: true,
        },
        useCustomSorting: {
            source: 'static',
            value: false,
        },
        availableSortings: {
            source: 'static',
            value: {},
        },
        defaultSorting: {
            source: 'static',
            value: '',
        },
        filters: {
            source: 'static',
            value: 'manufacturer-filter,rating-filter,price-filter,shipping-free-filter,property-filter',
        },
        propertyWhitelist: {
            source: 'static',
            value: [],
        },
    },

});

