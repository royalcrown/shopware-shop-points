import './preview';
import './component';
import './config';

Shopware.Service('cmsService').registerCmsElement({
    name: 'shop-point-map',
    label: 'sw-cms.elements.shop-point-map.label',
    component: 'sw-cms-el-shop-point-map',
    previewComponent: 'sw-cms-el-preview-shop-point-map',
    configComponent: 'sw-cms-el-config-shop-point-map',
    defaultConfig: {
        mapType: {
            source: 'static',
            value: 'roadmap'
        },
        zoom: {
            source: 'static',
            value: 8,
        },
        pinGlyphUrl: {
            source: 'static',
            value: null,
        },
        pinGlyphColor: {
            source: 'static',
            value: '#B31512'
        },
        pinBorderColor: {
            source: 'static',
            value: '#B31512',
        },
        pinBackgroundColor: {
            source: 'static',
            value: '#EA4335',
        },
        center: {
            source: 'static',
            value: '50.84895206110133, 4.357618657122824'
        },
        boxLayout: {
            source: 'static',
            value: 'standard',
        },
        displayMode: {
            source: 'static',
            value: 'standard',
        },
        verticalAlign: {
            source: 'static',
            value: null,
        },
        height: {
            source: 'static',
            value: '250px'
        }
    },
    defaultData: {
        boxLayout: 'standard',
        product: null,
    }
});
