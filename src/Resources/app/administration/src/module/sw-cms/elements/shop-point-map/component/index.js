import template from './sw-cms-el-shop-point-map.html.twig';
import './sw-cms-el-shop-point-map.scss';

const { Mixin, Filter } = Shopware;

Shopware.Component.register('sw-cms-el-shop-point-map', {
    template,

    mixins: [
        Mixin.getByName('cms-element'),
        Mixin.getByName('placeholder'),
    ],
    computed: {
      styles() {
          let height = {'min-height': '250px'};
          if(this.element.config.height.value && this.element.config.height.value !== 0) {
              height = {'min-height': this.element.config.height.value};
          }
          return {
              ...height
          }
      },
      imgStyles() {
        return  {'height': this.element.config.height.value};
      }
    },
    watch: {
        cmsPageState: {
            deep: true,
            handler() {
                this.$forceUpdate();
            },
        },
    },
    created() {
        this.createdComponent();
    },
    methods: {
        createdComponent() {
            this.initElementConfig('shop-point-map');
            this.initElementData('shop-point-map');
        },
    },
});
