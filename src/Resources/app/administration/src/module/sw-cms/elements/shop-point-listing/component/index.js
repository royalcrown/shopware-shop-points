import template from './sw-cms-el-shop-point-listing.html.twig';
import './sw-cms-el-shop-point-listing.scss';

const { Mixin } = Shopware;



Shopware.Component.register('sw-cms-el-shop-point-listing', {
    template,

    mixins: [
        Mixin.getByName('cms-element'),
    ],


    created() {
        this.createdComponent();
    },

    methods: {
        createdComponent() {
            this.initElementConfig('shop-point-listing');
        },
    },
});
