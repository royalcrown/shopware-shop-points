import template from './sw-cms-el-preview-shop-point-box.html.twig';
import './sw-cms-el-preview-shop-point-box.scss';

Shopware.Component.register('sw-cms-el-preview-shop-point-box', {
    template,
});
