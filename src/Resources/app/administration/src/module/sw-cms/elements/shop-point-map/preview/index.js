import template from './sw-cms-el-preview-shop-point-map.html.twig';
import './sw-cms-el-preview-shop-point-map.scss';

Shopware.Component.register('sw-cms-el-preview-shop-point-map', {
    template,
});
