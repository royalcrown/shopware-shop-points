import template from './sw-cms-shop-point-box-preview.html.twig';
import './sw-cms-shop-point-box-preview.scss';

Shopware.Component.register('sw-cms-shop-point-box-preview', {
    template,
});
