import template from './sw-cms-block-shop-point-listing.html.twig';
import './sw-cms-block-shop-point-listing.scss';

Shopware.Component.register('sw-cms-block-shop-point-listing', {
    template
});
