import './component';
import './preview';

Shopware.Service('cmsService').registerCmsBlock({
    name: 'shop-point-listing',
    label: 'sw-cms.blocks.commerce.shopPointListing.label',
    category: 'commerce',
    // hidden: true,
    // removable: false,
    component: 'sw-cms-block-shop-point-listing',
    previewComponent: 'sw-cms-preview-block-shop-point-listing',
    defaultConfig: {
        marginBottom: '20px',
        marginTop: '20px',
        marginLeft: '20px',
        marginRight: '20px',
        sizingMode: 'boxed',
    },
    slots: {
        content: 'shop-point-listing',
    },
});
