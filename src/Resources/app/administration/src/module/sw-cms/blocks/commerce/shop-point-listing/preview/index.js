import template from './sw-cms-preview-shop-point-listing.html.twig';
import './sw-cms-preview-shop-point-listing.scss';

Shopware.Component.register('sw-cms-preview-block-shop-point-listing', {
    template
});
