import './module/shop-points';
import './module/sw-cms/blocks/commerce/shop-point-listing';
import './module/sw-cms/component/sw-cms-shop-point-box-preview';
import './module/sw-cms/elements/shop-point-listing';
import './module/sw-cms/elements/shop-point-map';