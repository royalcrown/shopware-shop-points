<?php

declare(strict_types=1);

namespace ShopPoints\Storefront\Controller;

use ShopPoints\Storefront\Page\ShopPoint\ShopPointPageLoader;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\Feature;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use Shopware\Storefront\Controller\StorefrontController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: '/shop-point', defaults: ['_routeScope' => ['storefront']])]
class ShopPointController extends StorefrontController
{
    /**
     * Class constructor
     */
    public function __construct(
        private readonly ShopPointPageLoader $shopPointPageLoader
    ) {
    }

    #[Route(
        path: '/detail/{shopPointId}',
        name: 'frontend.detail.shopPoint.page',
        requirements: ['shopPointId' => '[a-zA-Z0-9_-]{32}'],
        defaults: ['_httpCache' => true],
        methods: ['GET']
    )]
    public function shopPointDetail(
        Context $context,
        Request $request,
        SalesChannelContext $salesChannelContext
    ): Response {
        $page = $this->shopPointPageLoader->load($request, $context, $salesChannelContext);

        if ($page->getCmsPage()) {
            return $this->renderStorefront('@Storefront/storefront/page/content/detail.html.twig', [
                    'cmsPage' => $page->getCmsPage(),
                ]);
        }

        return $this->renderStorefront('@ShopPoints/storefront/page/content/shop-point-detail.html.twig', [
            'page' => $page,
            ]);
    }

    #[Route(
        path: '/list',
        name: 'frontend.list.shopPoint.page',
        defaults: ['_httpCache' => true],
        methods: ['GET']
    )]
    public function shopPointList(
        Context $context,
        Request $request,
        SalesChannelContext $salesChannelContext
    ): Response {

        $page = $this->shopPointPageLoader->loadList($request, $context, $salesChannelContext);

        return $this->renderStorefront('@ShopPoints/storefront/page/content/shop-point-listing.html.twig', [
            'page' => $page,
        ]);
    }

}

