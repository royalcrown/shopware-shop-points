<?php declare(strict_types=1);

namespace ShopPoints\Storefront\Page\ShopPoint;

use ShopPoints\Core\Content\ShopPoint\ShopPoint;
use ShopPoints\Core\Content\ShopPoint\ShopPointDefinition;
use Shopware\Core\Content\Cms\CmsPageEntity;
use Shopware\Storefront\Page\Page;

class ShopPointPage extends Page
{
    /**
     * @var ShopPoint
     */
    protected $shopPoint;

    /**
     * @var CmsPageEntity
     */
    protected $cmsPage;

    public function getShopPoint(): ShopPoint
    {
        return $this->shopPoint;
    }

    public function setShopPoint(ShopPoint $shopPoint): void
    {
        $this->shopPoint = $shopPoint;
    }

    public function getCmsPage(): ?CmsPageEntity
    {
        return $this->cmsPage;
    }

    public function setCmsPage(CmsPageEntity $cmsPage): void
    {
        $this->cmsPage = $cmsPage;
    }

    public function getEntityName(): string
    {
        return ShopPointDefinition::ENTITY_NAME;
    }
}
