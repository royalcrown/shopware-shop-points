<?php

declare(strict_types=1);

namespace ShopPoints\Storefront\Page\ShopPoint;

use ShopPoints\Core\Content\ShopPoint\ShopPoint;
use ShopPoints\Core\Content\ShopPoint\ShopPointDefinition;
use Shopware\Core\Content\Cms\CmsPageEntity;
use Shopware\Core\Content\Cms\DataResolver\ResolverContext\EntityResolverContext;
use Shopware\Core\Content\Cms\SalesChannel\SalesChannelCmsPageLoaderInterface;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsAnyFilter;
use Shopware\Core\Framework\Routing\Exception\MissingRequestParameterException;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use Shopware\Storefront\Page\GenericPageLoaderInterface;
use Symfony\Component\HttpFoundation\Request;

class ShopPointPageLoader
{
    public function __construct(
        private readonly EntityRepository $shopPointRepository,
        private readonly GenericPageLoaderInterface $genericLoader,
        private readonly ShopPointDefinition $shopPointDefinition,
        private readonly SalesChannelCmsPageLoaderInterface $cmsPageLoader
    ) {
    }

    public const SHOP_POINT_LISTING = '018af54edec87f8da66f16d3a481f314';

    public function load(Request $request, Context $context, SalesChannelContext $salesChannelContext): ShopPointPage
    {
        $shopPointId = $request->attributes->get('shopPointId');
        if (!$shopPointId) {
            throw new MissingRequestParameterException('shopPointId', '/shopPointId');
        }

        $criteria = (new Criteria([$shopPointId]))
            ->addAssociation('media')
            ->addAssociation('mediaList')
            ->addAssociation('cover')
            ->addAssociation('translations')
            ->addAssociation('openingHoursMon.hours')
            ->addAssociation('openingHoursTue.hours')
            ->addAssociation('openingHoursWed.hours')
            ->addAssociation('openingHoursThu.hours')
            ->addAssociation('openingHoursFri.hours')
            ->addAssociation('openingHoursSat.hours')
            ->addAssociation('openingHoursSun.hours')
        ;

        $shopPoint = $this->shopPointRepository->search($criteria, $context)->first();

        $page = $this->genericLoader->load($request, $salesChannelContext);
        $page = ShopPointPage::createFrom($page);
        $page->setShopPoint($shopPoint);

        $this->checkCmsPage($shopPoint, $salesChannelContext, $request);

        if ($cmsPage = $shopPoint->getCmsPage()) {
            $page->setCmsPage($cmsPage);
        }

        return $page;
    }

    public function loadList(Request $request, Context $context, SalesChannelContext $salesChannelContext): ShopPointPage
    {
        $page = $this->genericLoader->load($request, $salesChannelContext);
        $page = ShopPointPage::createFrom($page);
        $page->setCmsPage($this->getCmsListingPage($salesChannelContext, $request));

        return $page;
    }

    private function checkCmsPage(ShopPoint $shopPoint, SalesChannelContext $context, Request $request): void
    {
        $pageId = $shopPoint->getCmsPageId();

        if ($pageId) {
            // clone product to prevent recursion encoding (see NEXT-17603)
            $resolverContext = new EntityResolverContext(
                $context,
                $request,
                $this->shopPointDefinition,
                clone $shopPoint
            );

            $pages = $this->cmsPageLoader->load(
                $request,
                $this->createCriteria($pageId, $request),
                $context,
                $shopPoint->getTranslation('slotConfig'),
                $resolverContext
            );

            if ($page = $pages->first()) {
                $shopPoint->setCmsPage($page);
            }
        }
    }

    private function getCmsListingPage(SalesChannelContext $context, Request $request): CmsPageEntity
    {
        $pages = $this->cmsPageLoader->load(
            $request,
            $this->createCriteria(self::SHOP_POINT_LISTING, $request),
            $context
        );

        return $pages->first();
    }

    private function createCriteria(string $pageId, Request $request): Criteria
    {
        $criteria = new Criteria([$pageId]);
        $criteria->setTitle('shop-point::cms-page');
        $slots = $request->get('slots');
        if (\is_string($slots)) {
            $slots = explode('|', $slots);
        }

        if (!empty($slots) && \is_array($slots)) {
            $criteria
                ->getAssociation('sections.blocks')
                ->addFilter(new EqualsAnyFilter('slots.id', $slots));
        }

        return $criteria;
    }
}
