<?php

namespace ShopPoints\Core\Content\OpeningHours;

use ShopPoints\Core\Content\OpeningHour\OpeningHourCollection;
use Shopware\Core\Framework\DataAbstractionLayer\Entity;
use Shopware\Core\Framework\DataAbstractionLayer\EntityIdTrait;

class OpeningHours extends Entity
{
    use EntityIdTrait;

    protected OpeningHourCollection $hours;


    public function __construct()
    {
        $this->hours = new OpeningHourCollection();
    }

    public function __toString(): string
    {
        $openingHoursCollection = [];
        foreach ($this->hours as $hour) {
            $trimmedStart = substr($hour->getStart(), 0, -3);
            $trimmedEnd = substr($hour->getEnd(), 0, -3);
            $openingHoursCollection[] = $trimmedStart . ' - ' . $trimmedEnd;
        }

        // Join collection into a string, separated by ' & '
        return $this->formatOpeningHours($openingHoursCollection);
    }
    protected function formatOpeningHours(array $openingHoursCollection): string
    {
        return implode(' & ', $openingHoursCollection);
    }

    public function getHours(): OpeningHourCollection
    {
        return $this->hours;
    }

    public function setHours(OpeningHourCollection $hours): void
    {
        $this->hours = $hours;
    }

}
