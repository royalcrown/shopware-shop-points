<?php

namespace ShopPoints\Core\Content\OpeningHours;

use Shopware\Core\Framework\DataAbstractionLayer\EntityCollection;

class OpeningHoursCollection extends EntityCollection
{
    protected function getExpectedClass(): string
    {
        return OpeningHours::class;
    }
}
