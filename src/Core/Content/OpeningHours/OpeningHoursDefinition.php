<?php

namespace ShopPoints\Core\Content\OpeningHours;

use ShopPoints\Core\Content\OpeningHour\OpeningHourDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\EntityDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\PrimaryKey;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\Required;
use Shopware\Core\Framework\DataAbstractionLayer\Field\IdField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\OneToManyAssociationField;
use Shopware\Core\Framework\DataAbstractionLayer\FieldCollection;

class OpeningHoursDefinition extends EntityDefinition
{
    public const ENTITY_NAME = 'rc_opening_hours';

    public function getEntityName(): string
    {
        return self::ENTITY_NAME;
    }

    protected function defineFields(): FieldCollection
    {
        return new FieldCollection([
            (new IdField('id', 'id'))->addFlags(new Required(), new PrimaryKey()),

            // associations
            (new OneToManyAssociationField('hours', OpeningHourDefinition::class, 'opening_hours_id')),
        ]);
    }

    public function getEntityClass(): string
    {
        return OpeningHours::class;
    }

    public function getCollectionClass(): string
    {
        return OpeningHoursCollection::class;
    }
}
