<?php declare(strict_types=1);

namespace ShopPoints\Core\Content\ShopPoint\Aggregate\ShopPointMedia;

use ShopPoints\Core\Content\ShopPoint\ShopPoint;
use Shopware\Core\Content\Media\MediaEntity;
use Shopware\Core\Framework\DataAbstractionLayer\Entity;
use Shopware\Core\Framework\DataAbstractionLayer\EntityCustomFieldsTrait;
use Shopware\Core\Framework\DataAbstractionLayer\EntityIdTrait;


class ShopPointMediaEntity extends Entity
{
    use EntityCustomFieldsTrait;
    use EntityIdTrait;

    /**
     * @var string
     */
    protected $shopPointId;

    /**
     * @var string
     */
    protected $mediaId;

    /**
     * @var int
     */
    protected $position;

    /**
     * @var MediaEntity|null
     */
    protected $media;

    /**
     * @var ShopPoint|null
     */
    protected $shopPoint;

    public function getShopPointId(): string
    {
        return $this->shopPointId;
    }

    public function setShopPointId(string $shopPointId): void
    {
        $this->shopPointId = $shopPointId;
    }

    public function getMediaId(): string
    {
        return $this->mediaId;
    }

    public function setMediaId(string $mediaId): void
    {
        $this->mediaId = $mediaId;
    }

    public function getPosition(): int
    {
        return $this->position;
    }

    public function setPosition(int $position): void
    {
        $this->position = $position;
    }

    public function getMedia(): ?MediaEntity
    {
        return $this->media;
    }

    public function setMedia(MediaEntity $media): void
    {
        $this->media = $media;
    }

    public function getShopPoint(): ?ShopPoint
    {
        return $this->shopPoint;
    }

    public function setShopPoint(ShopPoint $shopPoint): void
    {
        $this->shopPoint = $shopPoint;
    }
}
