<?php declare(strict_types=1);

namespace ShopPoints\Core\Content\ShopPoint\Aggregate\ShopPointMedia;

use Shopware\Core\Content\Media\MediaCollection;
use Shopware\Core\Framework\DataAbstractionLayer\EntityCollection;

/**
 * @extends EntityCollection<ShopPointMediaEntity>
 */

class ShopPointMediaCollection extends EntityCollection
{
    /**
     * @return list<string>
     */
    public function getShopPointIds(): array
    {
        return $this->fmap(fn (ShopPointMediaEntity $shopPointMedia) => $shopPointMedia->getShopPointId());
    }

    public function filterByProductId(string $id): self
    {
        return $this->filter(fn (ShopPointMediaEntity $shopPointMedia) => $shopPointMedia->getShopPointId() === $id);
    }

    /**
     * @return list<string>
     */
    public function getMediaIds(): array
    {
        return $this->fmap(fn (ShopPointMediaEntity $shopPointMedia) => $shopPointMedia->getMediaId());
    }

    public function filterByMediaId(string $id): self
    {
        return $this->filter(fn (ShopPointMediaEntity $shopPointMedia) => $shopPointMedia->getMediaId() === $id);
    }

    public function getMedia(): MediaCollection
    {
        return new MediaCollection(
            $this->fmap(fn (ShopPointMediaEntity $shopPointMedia) => $shopPointMedia->getMedia())
        );
    }

    public function getApiAlias(): string
    {
        return 'shop_point_media_collection';
    }

    protected function getExpectedClass(): string
    {
        return ShopPointMediaEntity::class;
    }
}
