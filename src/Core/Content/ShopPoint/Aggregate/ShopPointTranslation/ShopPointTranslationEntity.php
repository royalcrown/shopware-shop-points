<?php

declare(strict_types=1);

namespace ShopPoints\Core\Content\ShopPoint\Aggregate\ShopPointTranslation;

use ShopPoints\Core\Content\ShopPoint\ShopPoint;
use Shopware\Core\Framework\DataAbstractionLayer\EntityCustomFieldsTrait;
use Shopware\Core\Framework\DataAbstractionLayer\TranslationEntity;
class ShopPointTranslationEntity extends TranslationEntity
{
    use EntityCustomFieldsTrait;

    protected string $rcShopPointId;

    protected ShopPoint $rcShopPoint;

    protected $slotConfig;

    protected $description;

    public function getRcShopPointId(): string
    {
        return $this->rcShopPointId;
    }

    public function setRcShopPointId(string $rcShopPointId): void
    {
        $this->rcShopPointId = $rcShopPointId;
    }

    public function getRcShopPoint(): ?ShopPoint
    {
        return $this->rcShopPoint;
    }

    /**
     * @param ShopPoint $rcShopPoint
     */
    public function setRcShopPoint(ShopPoint $rcShopPoint): void
    {
        $this->rcShopPoint = $rcShopPoint;
    }

    public function getSlotConfig(): ?array
    {
        return $this->slotConfig;
    }

    public function setSlotConfig(array $slotConfig): void
    {
        $this->slotConfig = $slotConfig;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description): void
    {
        $this->description = $description;
    }


}
