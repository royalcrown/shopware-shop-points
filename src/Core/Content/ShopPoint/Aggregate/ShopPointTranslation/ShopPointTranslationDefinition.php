<?php

declare(strict_types=1);

namespace ShopPoints\Core\Content\ShopPoint\Aggregate\ShopPointTranslation;


use ShopPoints\Core\Content\ShopPoint\ShopPointDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\EntityTranslationDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\Field\CustomFields;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\ApiAware;
use Shopware\Core\Framework\DataAbstractionLayer\Field\JsonField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\LongTextField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\StringField;
use Shopware\Core\Framework\DataAbstractionLayer\FieldCollection;

class ShopPointTranslationDefinition extends EntityTranslationDefinition
{
    public const ENTITY_NAME = 'rc_shop_point_translation';

    public function getEntityName(): string
    {
        return self::ENTITY_NAME;
    }

    public function getCollectionClass(): string
    {
        return ShopPointTranslationCollection::class;
    }

    public function getEntityClass(): string
    {
        return ShopPointTranslationEntity::class;
    }

    public function getParentDefinitionClass(): string
    {
        return ShopPointDefinition::class;
    }

    protected function defineFields(): FieldCollection
    {
        return new FieldCollection([
            (new LongTextField('description', 'description'))->addFlags(new ApiAware()),
            (new JsonField('slot_config', 'slotConfig'))->addFlags(new ApiAware()),
            (new CustomFields())->addFlags(new ApiAware()),
        ]);
    }




}
