<?php

declare(strict_types=1);

namespace ShopPoints\Core\Content\ShopPoint\Aggregate\ShopPointTranslation;

use Shopware\Core\Framework\DataAbstractionLayer\EntityCollection;

class ShopPointTranslationCollection extends EntityCollection
{

    /**
     * @return list<string>
     */
    public function getLanguageIds(): array
    {
        return $this->fmap(fn (ShopPointTranslationEntity $shopPointTranslation) => $shopPointTranslation->getLanguageId());
    }

    public function filterByLanguageId(string $id): self
    {
        return $this->filter(fn (ShopPointTranslationEntity $shopPointTranslation) => $shopPointTranslation->getLanguageId() === $id);
    }
    protected function getExpectedClass(): string
    {
        return ShopPointTranslationEntity::class;
    }
}
