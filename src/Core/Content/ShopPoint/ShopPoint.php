<?php

declare(strict_types=1);

namespace ShopPoints\Core\Content\ShopPoint;

use ShopPoints\Core\Content\OpeningHours\OpeningHours;
use ShopPoints\Core\Content\ShopPoint\Aggregate\ShopPointMedia\ShopPointMediaCollection;
use ShopPoints\Core\Content\ShopPoint\Aggregate\ShopPointMedia\ShopPointMediaEntity;
use ShopPoints\Core\Content\ShopPoint\Aggregate\ShopPointTranslation\ShopPointTranslationCollection;
use Shopware\Core\Content\Cms\CmsPageEntity;
use Shopware\Core\Framework\DataAbstractionLayer\Entity;
use Shopware\Core\Framework\DataAbstractionLayer\EntityIdTrait;

/**
 * Class ShopPoint
 *
 * @author    Geert Huygen <Geert.huygen@royalcrown.be>
 * @copyright RoyalCrown <http://www.royalcrown.be>
 */
class ShopPoint extends Entity
{
    use EntityIdTrait;

    public const LOCATION_KEY = 'location';

    protected ?string $name;

    protected ?string $coordinates;

    protected ?string $description;

    protected bool $active = false;

    protected bool $highlighted = false;

    protected ?CmsPageEntity $cmsPage = null;

    protected ?string $cmsPageId = null;

    protected ?string $coverId = null;
    /**
     * @var ShopPointMediaEntity|null
     */
    protected $cover;

    /**
     * @var ShopPointTranslationCollection|null
     */
    protected $translations;

    protected ?string $openingHoursMonId;
    protected ?string $openingHoursTueId;
    protected ?string $openingHoursWedId;
    protected ?string $openingHoursThuId;
    protected ?string $openingHoursFriId;
    protected ?string $openingHoursSatId;
    protected ?string $openingHoursSunId;

    protected $openingHoursMon;
    protected $openingHoursTue;
    protected $openingHoursWed;
    protected $openingHoursThu;
    protected $openingHoursFri;
    protected $openingHoursSat;
    protected $openingHoursSun;

    protected ?string $street;
    protected ?string $number;
    protected ?string $zipCode;
    protected ?string $city;

    protected ?string $phoneNumber;
    protected ?string $contactEmail;
    protected ?string $nameContactPerson;

    /**
     * @var ShopPointMediaCollection|null
     */
    protected $mediaList;



    public function __construct()
    {
        $this->openingHoursMon = new OpeningHours();
        $this->openingHoursTue = new OpeningHours();
        $this->openingHoursWed = new OpeningHours();
        $this->openingHoursThu = new OpeningHours();
        $this->openingHoursFri = new OpeningHours();
        $this->openingHoursSat = new OpeningHours();
        $this->openingHoursSun = new OpeningHours();
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    public function getCoordinates(): ?string
    {
        return $this->coordinates;
    }

    public function setCoordinates(?string $coordinates): void
    {
        $this->coordinates = $coordinates;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): void
    {
        $this->description = $description;
    }

    public function isActive(): bool
    {
        return $this->active;
    }

    public function setActive(bool $active): void
    {
        $this->active = $active;
    }

    public function isHighlighted(): bool
    {
        return $this->highlighted;
    }

    public function setHighlighted(bool $highlighted): void
    {
        $this->highlighted = $highlighted;
    }

    public function getCmsPage(): ?CmsPageEntity
    {
        return $this->cmsPage;
    }

    public function setCmsPage(CmsPageEntity $cmsPage): void
    {
        $this->cmsPage = $cmsPage;
    }

    public function getCmsPageId(): ?string
    {
        return $this->cmsPageId;
    }

    public function setCmsPageId(string $cmsPageId): void
    {
        $this->cmsPageId = $cmsPageId;
    }

    public function getTranslations(): ?ShopPointTranslationCollection
    {
        return $this->translations;
    }

    public function setTranslations(ShopPointTranslationCollection $translations): void
    {
        $this->translations = $translations;
    }

    public function getCoverId(): ?string
    {
        return $this->coverId;
    }

    public function setCoverId(?string $coverId): void
    {
        $this->coverId = $coverId;
    }

    public function getCover(): ?ShopPointMediaEntity
    {
        return $this->cover;
    }

    public function setCover(ShopPointMediaEntity $cover): void
    {
        $this->cover = $cover;
    }

    public function getOpeningHoursMonId(): ?string
    {
        return $this->openingHoursMonId;
    }

    public function setOpeningHoursMonId(?string $openingHoursMonId): void
    {
        $this->openingHoursMonId = $openingHoursMonId;
    }

    public function getOpeningHoursTueId(): ?string
    {
        return $this->openingHoursTueId;
    }

    public function setOpeningHoursTueId(?string $openingHoursTueId): void
    {
        $this->openingHoursTueId = $openingHoursTueId;
    }

    public function getOpeningHoursWedId(): ?string
    {
        return $this->openingHoursWedId;
    }

    public function setOpeningHoursWedId(?string $openingHoursWedId): void
    {
        $this->openingHoursWedId = $openingHoursWedId;
    }

    public function getOpeningHoursThuId(): ?string
    {
        return $this->openingHoursThuId;
    }

    public function setOpeningHoursThuId(?string $openingHoursThuId): void
    {
        $this->openingHoursThuId = $openingHoursThuId;
    }

    public function getOpeningHoursFriId(): ?string
    {
        return $this->openingHoursFriId;
    }

    public function setOpeningHoursFriId(?string $openingHoursFriId): void
    {
        $this->openingHoursFriId = $openingHoursFriId;
    }

    public function getOpeningHoursSatId(): ?string
    {
        return $this->openingHoursSatId;
    }

    public function setOpeningHoursSatId(?string $openingHoursSatId): void
    {
        $this->openingHoursSatId = $openingHoursSatId;
    }

    public function getOpeningHoursSunId(): ?string
    {
        return $this->openingHoursSunId;
    }

    public function setOpeningHoursSunId(?string $openingHoursSunId): void
    {
        $this->openingHoursSunId = $openingHoursSunId;
    }

    public function getOpeningHoursMon(): ?OpeningHours
    {
        return $this->openingHoursMon;
    }

    public function setOpeningHoursMon(OpeningHours $openingHoursMon): void
    {
        $this->openingHoursMon = $openingHoursMon;
    }

    public function getOpeningHoursTue(): ?OpeningHours
    {
        return $this->openingHoursTue;
    }

    public function setOpeningHoursTue(OpeningHours $openingHoursTue): void
    {
        $this->openingHoursTue = $openingHoursTue;
    }

    public function getOpeningHoursWed(): ?OpeningHours
    {
        return $this->openingHoursWed;
    }

    public function setOpeningHoursWed(OpeningHours $openingHoursWed): void
    {
        $this->openingHoursWed = $openingHoursWed;
    }

    public function getOpeningHoursThu(): ?OpeningHours
    {
        return $this->openingHoursThu;
    }

    public function setOpeningHoursThu(OpeningHours $openingHoursThu): void
    {
        $this->openingHoursThu = $openingHoursThu;
    }

    public function getOpeningHoursFri(): ?OpeningHours
    {
        return $this->openingHoursFri;
    }

    public function setOpeningHoursFri(OpeningHours $openingHoursFri): void
    {
        $this->openingHoursFri = $openingHoursFri;
    }

    public function getOpeningHoursSat(): ?OpeningHours
    {
        return $this->openingHoursSat;
    }

    public function setOpeningHoursSat(OpeningHours $openingHoursSat): void
    {
        $this->openingHoursSat = $openingHoursSat;
    }

    public function getOpeningHoursSun(): ?OpeningHours
    {
        return $this->openingHoursSun;
    }

    public function setOpeningHoursSun(OpeningHours $openingHoursSun): void
    {
        $this->openingHoursSun = $openingHoursSun;
    }

    public function isBetweenOpeningHours(): bool
    {
        $now = new \DateTime();
        $day = $now->format('D');
        $openingHours = $this->{'openingHours'.$day};

        if (!$openingHours) {
            return false;
        }

        foreach ($openingHours->getHours() as $openingHour) {
            $startDate = \DateTimeImmutable::createFromFormat('H:i:s', $openingHour->getStart());
            $endDate = \DateTimeImmutable::createFromFormat('H:i:s', $openingHour->getEnd());

            if ($startDate <= $now && $endDate >= $now) {
                return true;
            }
        }

        return false;
    }

    public function getStreet(): ?string
    {
        return $this->street;
    }

    public function setStreet(?string $street): void
    {
        $this->street = $street;
    }

    public function getNumber(): ?string
    {
        return $this->number;
    }

    public function setNumber(?string $number): void
    {
        $this->number = $number;
    }

    public function getZipCode(): ?string
    {
        return $this->zipCode;
    }

    public function setZipCode(?string $zipCode): void
    {
        $this->zipCode = $zipCode;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(?string $city): void
    {
        $this->city = $city;
    }

    public function getPhoneNumber(): ?string
    {
        return $this->phoneNumber;
    }

    public function setPhoneNumber(?string $phoneNumber): void
    {
        $this->phoneNumber = $phoneNumber;
    }

    public function getContactEmail(): ?string
    {
        return $this->contactEmail;
    }

    public function setContactEmail(?string $contactEmail): void
    {
        $this->contactEmail = $contactEmail;
    }

    public function getNameContactPerson(): ?string
    {
        return $this->nameContactPerson;
    }

    public function setNameContactPerson(?string $nameContactPerson): void
    {
        $this->nameContactPerson = $nameContactPerson;
    }

    public function getMediaList(): ?ShopPointMediaCollection
    {
        return $this->mediaList;
    }

    public function setMediaList(ShoppointMediaCollection $mediaList): void
    {
        $this->cover = $mediaList;
    }
}
