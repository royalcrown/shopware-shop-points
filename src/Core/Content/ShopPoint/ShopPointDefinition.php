<?php declare(strict_types=1);

namespace ShopPoints\Core\Content\ShopPoint;

use ShopPoints\Core\Content\OpeningHours\OpeningHoursDefinition;
use ShopPoints\Core\Content\ShopPoint\Aggregate\ShopPointMedia\ShopPointMediaDefinition;
use ShopPoints\Core\Content\ShopPoint\Aggregate\ShopPointTranslation\ShopPointTranslationDefinition;
use Shopware\Core\Content\Cms\CmsPageDefinition;
use Shopware\Core\Content\Media\MediaDefinition;
use Shopware\Core\Content\Product\Aggregate\ProductMedia\ProductMediaDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\EntityDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\Field\BoolField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\FkField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\ApiAware;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\CascadeDelete;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\Inherited;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\NoConstraint;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\PrimaryKey;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\Required;
use Shopware\Core\Framework\DataAbstractionLayer\Field\IdField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\ManyToOneAssociationField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\OneToManyAssociationField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\OneToOneAssociationField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\ReferenceVersionField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\StringField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\TranslatedField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\TranslationsAssociationField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\VersionField;
use Shopware\Core\Framework\DataAbstractionLayer\FieldCollection;

class ShopPointDefinition extends EntityDefinition
{
    public const ENTITY_NAME = 'rc_shop_point';

    final public const CONFIG_KEY_DEFAULT_CMS_PAGE_SHOP_POINT = 'rc.cms.default_shop_point_cms_page';

    public function getEntityName(): string
    {
        return self::ENTITY_NAME;
    }

    public function since(): ?string
    {
        return '6.0.0.0';
    }

    protected function defineFields(): FieldCollection
    {
        return new FieldCollection([
            (new IdField('id', 'id'))->addFlags(new Required(), new PrimaryKey()),
            (new StringField('name', 'name')),
            (new StringField('coordinates', 'coordinates')),
            (new StringField('street', 'street')),
            (new StringField('number', 'number')),
            (new StringField('zip_code', 'zipCode')),
            (new StringField('city', 'city')),
            (new StringField('phone_number', 'phoneNumber')),
            (new StringField('contact_email', 'contactEmail')),
            (new StringField('name_contact_person', 'nameContactPerson')),
            (new BoolField('active', 'active')),
            (new BoolField('highlighted', 'highlighted')),
            (new FkField('cms_page_id', 'cmsPageId', CmsPageDefinition::class))->addFlags(new ApiAware(), new Inherited()),
            (new FkField('shop_point_media_id', 'coverId', ShopPointMediaDefinition::class))->addFlags(new ApiAware(), new Inherited(), new NoConstraint()),

            (new ReferenceVersionField(CmsPageDefinition::class))->addFlags(new Inherited(), new Required(), new ApiAware()),
            (new TranslatedField('slotConfig'))->addFlags(new ApiAware()),

            (new FkField('opening_hours_mon_id', 'openingHoursMonId', OpeningHoursDefinition::class))->addFlags(new ApiAware()),
            (new FkField('opening_hours_tue_id', 'openingHoursTueId', OpeningHoursDefinition::class))->addFlags(new ApiAware()),
            (new FkField('opening_hours_wed_id', 'openingHoursWedId', OpeningHoursDefinition::class))->addFlags(new ApiAware()),
            (new FkField('opening_hours_thu_id', 'openingHoursThuId', OpeningHoursDefinition::class))->addFlags(new ApiAware()),
            (new FkField('opening_hours_fri_id', 'openingHoursFriId', OpeningHoursDefinition::class))->addFlags(new ApiAware()),
            (new FkField('opening_hours_sat_id', 'openingHoursSatId', OpeningHoursDefinition::class))->addFlags(new ApiAware()),
            (new FkField('opening_hours_sun_id', 'openingHoursSunId', OpeningHoursDefinition::class))->addFlags(new ApiAware()),

            // associations
            new ManyToOneAssociationField(
                'cmsPage',
                'cms_page_id',
                CmsPageDefinition::class,
                'id',
            true
            ),

            new OneToOneAssociationField('openingHoursMon', 'opening_hours_mon_id','id', OpeningHoursDefinition::class,true),
            new OneToOneAssociationField('openingHoursTue', 'opening_hours_tue_id','id', OpeningHoursDefinition::class,true),
            new OneToOneAssociationField('openingHoursWed', 'opening_hours_wed_id','id', OpeningHoursDefinition::class,true),
            new OneToOneAssociationField('openingHoursThu', 'opening_hours_thu_id','id', OpeningHoursDefinition::class,true),
            new OneToOneAssociationField('openingHoursFri', 'opening_hours_fri_id','id', OpeningHoursDefinition::class,true),
            new OneToOneAssociationField('openingHoursSat', 'opening_hours_sat_id','id', OpeningHoursDefinition::class,true),
            new OneToOneAssociationField('openingHoursSun', 'opening_hours_sun_id','id', OpeningHoursDefinition::class,true),

            (new ManyToOneAssociationField('cover', 'shop_point_media_id', ShopPointMediaDefinition::class, 'id'))->addFlags(new ApiAware()),
            (new OneToManyAssociationField('mediaList', ShopPointMediaDefinition::class, 'shop_point_id'))->addFlags(new ApiAware(), new CascadeDelete()),

            (new TranslatedField('description'))->addFlags(new ApiAware()),

            (new TranslationsAssociationField(
                ShopPointTranslationDefinition::class,
                'rc_shop_point_id'
            ))->addFlags(new ApiAware(), new Required()),

        ]);
    }

    public function getEntityClass(): string
    {
        return ShopPoint::class;
    }

    public function getCollectionClass(): string
    {
        return ShopPointCollection::class;
    }
}
