<?php

declare(strict_types=1);

namespace ShopPoints\Core\Content\ShopPoint;

use Shopware\Core\Framework\DataAbstractionLayer\EntityCollection;

/**
 * Class ShopPointCollection
 *
 * @author    Geert Huygen <Geert.huygen@royalcrown.be>
 * @copyright RoyalCrown <http://www.royalcrown.be>
 *
 * @method void               add(ShopPoint $entity)
 * @method void               set(string $key, ShopPoint $entity)
 * @method ShopPoint[]    getIterator()
 * @method ShopPoint[]    getElements()
 * @method ShopPoint|null get(string $key)
 * @method ShopPoint|null first()
 * @method ShopPoint|null last()
 */
class ShopPointCollection extends EntityCollection
{
    protected function getExpectedClass(): string
    {
        return ShopPoint::class;
    }
}
