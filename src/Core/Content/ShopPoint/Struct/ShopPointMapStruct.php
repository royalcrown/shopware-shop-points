<?php

namespace ShopPoints\Core\Content\ShopPoint\Struct;

use Shopware\Core\Framework\DataAbstractionLayer\Search\EntitySearchResult;
use Shopware\Core\Framework\Log\Package;
use Shopware\Core\Framework\Struct\Struct;

#[Package('content')]
class ShopPointMapStruct extends Struct
{
    /**
     * @var array|null
     */
    protected $settings;
    /**
     * @var array|null
     */
    protected $shopPointsInfo;

    /**
     * @return array|null
     */
    public function getShopPointsInfo(): ?array
    {
        return $this->shopPointsInfo;
    }

    /**
     * @param array|null $shopPointsInfo
     */
    public function setShopPointsInfo(?array $shopPointsInfo): void
    {
        $this->shopPointsInfo = $shopPointsInfo;
    }

    public function getSettings(): ?array
    {
        return $this->settings;
    }

    public function setSettings(array $settings): void
    {
        $this->settings = $settings;
    }
}
