<?php declare(strict_types=1);

namespace ShopPoints\Core\Content\ShopPoint\Struct;

use Shopware\Core\Framework\DataAbstractionLayer\Search\EntitySearchResult;

use Shopware\Core\Framework\Struct\Struct;

class ShopPointListingStruct extends Struct
{
    /**
     * @var EntitySearchResult|null
     */
    protected $listing;

    public function getListing(): ?EntitySearchResult
    {
        return $this->listing;
    }

    public function setListing(EntitySearchResult $listing): void
    {
        $this->listing = $listing;
    }

    public function getApiAlias(): string
    {
        return 'cms_shop_point_listing';
    }
}
