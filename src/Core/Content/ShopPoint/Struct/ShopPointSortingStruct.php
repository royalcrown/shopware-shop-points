<?php

namespace ShopPoints\Core\Content\ShopPoint\Struct;

use Shopware\Core\Framework\Struct\Struct;

class ShopPointSortingStruct extends Struct
{
    protected array $options;
    protected string $currentSort = 'name-asc';

    /**
     * @return string
     */
    public function getCurrentSort(): string
    {
        return $this->currentSort;
    }

    /**
     * @param string $currentSort
     */
    public function setCurrentSort(string $currentSort): void
    {
        $this->currentSort = $currentSort;
    }

    /**
     * @return array
     */
    public function getOptions(): array
    {
        return $this->options;
    }

    /**
     * @param array $options
     */
    public function setOptions(array $options): void
    {
        $this->options = $options;
    }

    public function addSortingOption($sortingOption): void
    {
        $this->options[] = $sortingOption;
    }
}