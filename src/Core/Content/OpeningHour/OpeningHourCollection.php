<?php

namespace ShopPoints\Core\Content\OpeningHour;

use Shopware\Core\Framework\DataAbstractionLayer\EntityCollection;

class OpeningHourCollection extends EntityCollection
{
    protected function getExpectedClass(): string
    {
        return OpeningHour::class;
    }
}