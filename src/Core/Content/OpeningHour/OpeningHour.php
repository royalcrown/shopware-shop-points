<?php

namespace ShopPoints\Core\Content\OpeningHour;

use ShopPoints\Core\Content\OpeningHours\OpeningHours;
use Shopware\Core\Framework\DataAbstractionLayer\Entity;
use Shopware\Core\Framework\DataAbstractionLayer\EntityIdTrait;

class OpeningHour extends Entity
{
    use EntityIdTrait;

    protected ?string $start;

    protected ?string $end;

    protected ?string $openingHoursId;

    protected ?OpeningHours $openingHours;

    public function getStart(): string
    {
        return $this->start;
    }

    public function setStart(string $start): void
    {
        $this->start = $start;
    }

    public function getEnd(): string
    {
        return $this->end;
    }

    public function setEnd(string $end): void
    {
        $this->end = $end;
    }

    public function getOpeningHoursId(): ?string
    {
        return $this->openingHoursId;
    }

    public function setOpeningHoursId(?string $openingHoursId): void
    {
        $this->openingHoursId = $openingHoursId;
    }

    public function getOpeningHours(): ?OpeningHours
    {
        return $this->openingHours;
    }

    public function setOpeningHours(?OpeningHours $openingHours): void
    {
        $this->openingHours = $openingHours;
    }
}
