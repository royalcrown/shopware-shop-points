<?php

namespace ShopPoints\Core\Content\OpeningHour;

use ShopPoints\Core\Content\OpeningHours\OpeningHoursDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\EntityDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\Field\FkField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\ApiAware;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\PrimaryKey;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\Required;
use Shopware\Core\Framework\DataAbstractionLayer\Field\IdField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\ManyToOneAssociationField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\StringField;
use Shopware\Core\Framework\DataAbstractionLayer\FieldCollection;

class OpeningHourDefinition extends EntityDefinition
{
    public const ENTITY_NAME = 'rc_opening_hour';

    public function getEntityName(): string
    {
        return self::ENTITY_NAME;
    }

    protected function defineFields(): FieldCollection
    {
        return new FieldCollection([
            (new IdField('id', 'id'))->addFlags(new Required(), new PrimaryKey()),
            (new FkField('opening_hours_id', 'openingHoursId', OpeningHoursDefinition::class))->addFlags(new ApiAware()),
            (new StringField('start', 'start'))->addFlags(new ApiAware()),
            (new StringField('end', 'end'))->addFlags(new ApiAware()),

            // associations
            (new ManyToOneAssociationField('openingHours', 'opening_hours_id', OpeningHoursDefinition::class, 'id'))->addFlags(new ApiAware()),
        ]);
    }

    public function getEntityClass(): string
    {
        return OpeningHour::class;
    }

    public function getCollectionClass(): string
    {
        return OpeningHourCollection::class;
    }
}
