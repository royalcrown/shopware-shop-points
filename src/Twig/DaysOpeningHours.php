<?php

namespace ShopPoints\Twig;

use ShopPoints\Core\Content\OpeningHours\OpeningHours;
use ShopPoints\Core\Content\ShopPoint\ShopPoint;
use Shopware\Core\Framework\Adapter\Translation\Translator;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class DaysOpeningHours extends AbstractExtension
{
    private const DAYS = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'];

    public function __construct(private readonly Translator $translator)
    {
    }

    public function getFunctions()
    {
        return [
            new TwigFunction('getOpeningHoursArray', [$this, 'getOpeningHoursArray']),
        ];
    }

    public function getOpeningHoursArray(ShopPoint $shopPoint): array
    {
        $openingHoursArray = [];
        foreach (self::DAYS as $day) {
            $openingHoursArray[$this->translator->trans('rc-shop-point.days-short.' . $day)] = $this->createDayString($this->getOpeningHoursForDay($shopPoint, $day));
        }
        return $openingHoursArray;
    }

    private function createDayString(?OpeningHours $openingHours): string
    {
        $openingHoursString = (string)$openingHours;
        if (empty($openingHours)) {
            $openingHoursString = $this->translator->trans('rc-shop-point.closed');
        }
        return $openingHoursString;
    }

    private function getOpeningHoursForDay(ShopPoint $shopPoint, string $day): ?OpeningHours
    {
        $methodName = 'getOpeningHours' . ucfirst($day);
        return $shopPoint->$methodName();
    }
}
