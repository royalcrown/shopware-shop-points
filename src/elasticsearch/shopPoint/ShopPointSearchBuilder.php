<?php declare(strict_types=1);

namespace ShopPoints\elasticsearch\shopPoint;

use ShopPoints\Core\Content\ShopPoint\ShopPoint;
use ShopPoints\Core\Content\ShopPoint\Struct\ShopPointSortingStruct;
use Shopware\Core\Content\Product\SalesChannel\Listing\Filter;
use Shopware\Core\Content\Product\SalesChannel\Sorting\ProductSortingEntity;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\ContainsFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Query\ScoreQuery;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Sorting\FieldSorting;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Term\Tokenizer;
use Shopware\Core\Framework\Plugin\Exception\DecorationPatternException;
use Shopware\Core\Framework\Routing\RoutingException;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use Shopware\Core\System\SystemConfig\SystemConfigService;
use Symfony\Component\HttpFoundation\Request;

class ShopPointSearchBuilder
{
    public function __construct(
        private readonly Tokenizer $tokenizer,
        private readonly SystemConfigService $systemConfigService,
    ) {
    }

    public function init(): Criteria
    {
        //default criteria setup
        $criteria = new Criteria();
        $criteria->addState(Criteria::STATE_ELASTICSEARCH_AWARE);
        $criteria->addAssociation('cover.media');
        $criteria->addAssociation('openingHoursThu');
        $criteria->addAssociation('openingHoursThu.hours');
        $criteria->addFilter(new EqualsFilter('active', 1));

        return $criteria;
    }

    public function addSearchCriteria(Criteria $criteria, Request $request): void
    {
        $search = $request->get('search');

        if (!$search) {
            return;
        }

        if (\is_array($search)) {
            $term = implode(' ', $search);
        } else {
            $term = (string)$search;
        }

        $term = trim($term);
        if (empty($term)) {
            throw RoutingException::missingRequestParameter('search');
        }

        $tokens = $this->tokenizer->tokenize($term);

        foreach ($tokens as $token) {
            $criteria->addQuery(
                new ScoreQuery(
                    new ContainsFilter('name', $token),
                    12
                )
            );
        }
    }

    public function getDecorated(): self
    {
        throw new DecorationPatternException(self::class);
    }

    public function addDefaultSorting(Criteria $criteria, Request $request): void
    {
        // only set default order if no ordering is asked
        // or no location found (default sorting is name ascending)
        if ($request->get('order') || $request->cookies->get(ShopPoint::LOCATION_KEY)) {
            return;
        }

        $criteria->addSorting(new FieldSorting('name', FieldSorting::ASCENDING));
        $request->query->add(['order'=> 'name-asc']);
    }

    public function handleSorting(Criteria $criteria, Request $request, ShopPointSortingStruct $sortingConfig): void
    {
        //Geo distance sort is handled in a subscriber, needs direct access to the ES search
        $orderValue = $request->get('order');

        if (!$orderValue) {
            return;
        }

        $sorting = $this->findSortingByKey($orderValue, $sortingConfig);

        if (!$sorting) {
            return;
        }

        $sortingFields =  $sorting->createDalSorting();

        foreach ($sortingFields as $sortingField) {
            $criteria->addSorting($sortingField);
        }
    }

    private function findSortingByKey(string $key, ShopPointSortingStruct $sortingConfig): ?ProductSortingEntity
    {
        foreach ($sortingConfig->getOptions() as $option) {

            //skip location, handled in subscriber
            if ($option->getKey() === ShopPoint::LOCATION_KEY) {
                continue;
            }

            if ($option instanceof ProductSortingEntity && $option->getKey() === $key) {
                return $option;
            }
        }

        return null;
    }

    public function handlePagination(
        Criteria $criteria,
        SalesChannelContext $salesChannelContext,
        Request $request
    ): void {
        $limit = $this->getLimit($request, $salesChannelContext);
        $page = $this->getPage($request);

        $criteria->setOffset(($page - 1) * $limit);
        $criteria->setLimit($limit);
        $criteria->setTotalCountMode(Criteria::TOTAL_COUNT_MODE_EXACT);
    }

    private function getLimit(Request $request, SalesChannelContext $context): int
    {
        $limit = $request->query->getInt('limit', 0);

        if ($request->isMethod(Request::METHOD_POST)) {
            $limit = $request->request->getInt('limit', $limit);
        }

        $limit = $limit > 0 ? $limit : $this->systemConfigService->getInt('core.listing.productsPerPage', $context->getSalesChannel()->getId());

        return $limit <= 0 ? 24 : $limit;
    }

    private function getPage(Request $request): int
    {
        $page = $request->query->getInt('p', 1);

        if ($request->isMethod(Request::METHOD_POST)) {
            $page = $request->request->getInt('p', $page);
        }

        return $page <= 0 ? 1 : $page;
    }
}
