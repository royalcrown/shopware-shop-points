<?php declare(strict_types=1);

namespace ShopPoints\elasticsearch\shopPoint;

use Doctrine\DBAL\ArrayParameterType;
use Doctrine\DBAL\Connection;
use ShopPoints\Core\Content\ShopPoint\ShopPointDefinition;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityDefinition;
use Shopware\Elasticsearch\Framework\AbstractElasticsearchDefinition;

class EsShopPointDefinition extends AbstractElasticsearchDefinition
{

    /**
     * @param array<string, string> $languageAnalyzerMapping
     */
    public function __construct(
        private readonly ShopPointDefinition $definition,
        private readonly Connection $connection,
        private readonly array $languageAnalyzerMapping
    ) {
    }

    public function getEntityDefinition(): EntityDefinition
    {
        return $this->definition;
    }

    /**
     * {@inheritdoc}
     */
    public function getMapping(Context $context): array
    {
        $languages = $this->connection->fetchAllKeyValue(
            'SELECT LOWER(HEX(language.`id`)) as id, locale.code
             FROM language
             INNER JOIN locale ON locale_id = locale.id'
        );

        $languageFields = [];

        foreach ($languages as $languageId => $code) {
            $parts = explode('-', $code);
            $locale = $parts[0];

            $languageFields[$languageId] = self::getTextFieldConfig();
            if (\array_key_exists($locale, $this->languageAnalyzerMapping)) {
                $fields = $languageFields[$languageId]['fields'];
                $fields['search']['analyzer'] = $this->languageAnalyzerMapping[$locale];
                $languageFields[$languageId]['fields'] = $fields;
            }
        }

        $properties = [
            'id' => self::KEYWORD_FIELD,
            'name' => self::getTextFieldConfig(),
            'active' => self::BOOLEAN_FIELD,
            'coordinates' => [
                'type' => 'geo_point',
            ],
        ];

        return [
            '_source' => ['includes' => ['id', 'name', 'coordinates']],
            'properties' => $properties,
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function fetch(array $ids, Context $context): array
    {
        $data = $this->fetchData($ids, $context);
        $documents = [];

        foreach ($data as $id => $item) {
            $translations = (array) json_decode($item['translation'] ?? '[]', true, 512, \JSON_THROW_ON_ERROR);

            $document = [
                'id' => $id,
                'name' => $item['name'],
                'coordinates' => $item['coordinates'],
                'active' => (bool) $item['active'],
            ];

            $documents[$id] = $document;
        }

        return $documents;
    }

    private function fetchData(array $ids, Context $context): array
    {
        $sql = <<<'SQL'
SELECT
    LOWER(HEX(p.id)) AS id,
    p.name as name,
    p.coordinates as coordinates,
    p.active as active

FROM rc_shop_point p

WHERE p.id IN (:ids)

GROUP BY p.id
SQL;

        /** @var array<string, array<string, string>> $result */
        $result = $this->connection->fetchAllAssociativeIndexed(
            $sql,
            [
                'ids' => $ids,
            ],
            [
                'ids' => ArrayParameterType::STRING,
            ]
        );

        return $result;
    }
}
